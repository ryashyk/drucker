$(document).ready(function() {



    $(document).on('change','.main-product form',function (e) {
       e.preventDefault();
        self = $(this);
        console.log(self.serialize());
    });




        var fileInput = $("#upload_product_photo input[type=file]");

        if (fileInput.length){
            fileInput.change(function () {
                var formData = new FormData();
                $.each($('#upload_product_photo input'), function (i, inp) {

                    console.log($(inp).get(0).files);

                    var inp = $(inp);

                    if (!$(inp).get(0).files){
                        formData.append(inp.attr("name"), inp.val());
                    }
                    else formData.append(inp.attr("name"), inp.get(0).files[0]);
                });
                $.ajax({
                    'url': '/ajax',
                    'type': 'post',
                    'dataType': 'json',
                    contentType: false,
                    processData: false,
                    'data': formData
                }).done(function (resp) {
                    console.log(resp);
                    if (resp.status){
                        $(".frame").append('<img id="sample_picture"  src="'+resp.photo+'">');


                        // Guillotine loader
                        $('#sample_picture').on('load', function() {
                            var img = $(this);

                            // Remove any existing instance
                            if (img.guillotine('instance')) img.guillotine('remove')

                            // Create new instance
                            img.guillotine({width: 400, height: 300})

                            // Initialize plugin (with custom event)
                            img.guillotine({eventOnChange: 'guillotinechange'});

                            // Display inital data
                            var data = img.guillotine('getData');


                            // Bind button actions
                            $(document).on('click','#rotate_left',function(){
                                img.guillotine('rotateLeft');
                            });

                            $(document).on('click','#rotate_right',function(){
                                img.guillotine('rotateRight');
                            });

                            $(document).on('click','#fit',function(){
                                img.guillotine('fit');
                            });


                            $(document).on('click','#zoom_in',function(){
                                img.guillotine('zoomIn');
                            });


                            $(document).on('click','#zoom_out',function(){
                                img.guillotine('zoomOut');
                            });

                            // Update data on change
                            img.on('guillotinechange', function(ev, data, action) {
                                data.scale = parseFloat(data.scale.toFixed(4));
                                for(var k in data) { $('#'+k).html(data[k]); }
                            });
                        });


                    }

                });
            });
        }


    $('#cart_orders').on('submit',function (e) {
        e.preventDefault();
        var data_ = $('#cart_orders').serialize();

        $.ajax({
            type: "POST",
            url: "/ajax",
            data: data_,
            success: function (data) {
                $('#paypal').trigger('submit');
                // window.location.href = '/profile_user';
            }
        });
    });



    $(document).on("submit", "#cr_form", function(event) {
        event.preventDefault();
        var data_ = $('#cr_form').serialize();

        $.ajax({
            type: "POST",
            url: "/ajax",
            data: data_,
            success: function (data) {
                $('.elements ul li a span').html(data.total);
                    window.location.href = '/cart';
            }
        });
    });

    $(document).on('click','#trigger_cart',function (e) {
        e.preventDefault();
        $('#cr_form').trigger('submit');
    });

    $(document).on('submit','.form_open_login',function (e) {
        e.preventDefault();
        var form = $('.form_open_login');
        var data_ = $('.form_open_login').serialize();
        $.ajax({
            type: "POST",
            url: "/ajax",
            data: data_,
            success: function (data) {
                form.find("p.error").remove();
                if (data.status){
                    $("#message").html(data.msg);
                    location.reload();
                }
                else {
                    for (fieldName in data.error) {
                        form.find("[name = " + fieldName + "]").after( "<p class='error'>" + data.error[fieldName] + "</p>" );
                    }
                }
            }
        });
    });
    $("#registration_form").submit(function( event ) {
        event.preventDefault();

        var form = $(this);
        var action = form.attr("data-action");
        var data = form.serialize();

        $.ajax({
            type: "POST",
            url: action,
            data: data,
            success: function (data) {
                form.find("p.error").remove();
                $("#message").html("");
                if (data.status){
                    window.location.replace("/profile_user");
                }
                else {
                    for (fieldName in data.error) {
                        var field = form.find("[name = " + fieldName + "]");
                        if (field.next().hasClass("select2")) field.next().after( "<p class='error'>" + data.error[fieldName] + "</p>" );
                        else field.after( "<p class='error'>" + data.error[fieldName] + "</p>" );
                    }
                    if (data.msg) $("#message").html(data.msg);
                }
            }
        });
    });


});
