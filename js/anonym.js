$(document).ready(function() {

	if ($('.opened_menu').length>0) {
		$('.opened_menu .menus ul li').css('width', '100%');
	}

	$('.open_search').on('click', function(event) {
		event.preventDefault();
		console.log('test');
		$('#search_form').toggle();
	});

	$('.big_slider-wrp').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.big_slider-wrp').slick({
		arrows:false,
		dots:true,
		autoplay: true,
  		autoplaySpeed: 5000,
	});


    var options = {
        horizontal: 1,
        itemNav: 'basic',
        speed: 300,
        mouseDragging: 1,
        touchDragging: 1,
        forward:  null, // Selector or DOM element for "forward movement" button.
        backward: null, // Selector or DOM element for "backward movement" button.
        prev:     null, // Selector or DOM element for "previous item" button.
        next:     null, // Selector or DOM element for "next item" button.
        prevPage: null, // Selector or DOM element for "previous page" button.
        dragHandle: 1,
        scrollBar: $('.header_menu .scrollbar'),
        scrollBy: 1,
        clickBar: 1,
        nextPage: null // Selector or DOM element for "next page" button.

    };
    var frame = new Sly('.frame_bl', options).init();



	$('.menu_to_open').hover(function () {
	    var $t = $(this);
	    var id = $t.data('id');
	    var $hiddenID = $('[data-hidden-id="'+id+'"]');
        $hiddenID.show();
	} , function () {
        $('[data-hidden-id]').hide();
	})

	if ($(window).width()<1199) {
		$('.menu>ul>li:nth-child(n+2) a').on('click', function(event) {
			event.preventDefault();
			console.log('test');
			$(this).siblings('.hidden_menu').toggle();
		});
	}


	$('.aanbevolen_product-carousel').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.aanbevolen_product-carousel').slick({
		  dots: false,
		  arrows:true,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
	});

	$('.populairste_producten-carousel').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.populairste_producten-carousel').slick({
		  dots: false,
		  arrows:true,
		  slidesToShow: 6,
		  slidesToScroll: 6,
		  responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 5,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 4
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
	});

	$('.comments-carousel').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.comments-carousel').slick({
		arrows:true,
		dots:false,
		adaptiveHeight: true,
        slidesToShow: 1,
	});

	$('.links .row div').each(function(index, el) {
		if($(this).height()>238) {
			$(this).css({
				'height' : '238px',
				'overflow' : 'hidden',
			});
			$(this).append("<span><a href='' class='more_links'>more...</a><span>");
		};
	});
	$('.more_links').on('click', function(event) {
		event.preventDefault();
		$(this).closest('span').parent('div').css({
			'height' : 'auto'
		});
		$(this).closest('span').remove();
	});

	$('.open_menu').on('click', function(event) {
		event.preventDefault();
		$(this).siblings('ul').slideToggle();
	});

	/*    map      */

	var map;
     	function initMap() {
     		 var img = {
		      url: '/img/pin.png',
		    };
     		var myLatlng = new google.maps.LatLng(52.0009833, 4.1613421);
     		var mapOptions = {
		      zoom: 18,
		      scrollwheel: false,
		      center: myLatlng ,
		    };
			map = new google.maps.Map(document.getElementById('map'),mapOptions );
			var marker = new google.maps.Marker({
		      position: myLatlng,
		      map: map,
		      icon: img,
		    });
		};
		if ($('#map').length > 0) {
			initMap();
		}

  		/*  select   */

  		$(".js-example-basic-single").select2();

  		// change location in some time
		   /*setTimeout('location.replace("/html/main.html")', 3000);
			$(window).on('load', function() {
			      setTimeout('location.replace("/html/main.html")', 3000);
			});*/

		//tab transitor link

		/*$('#transitor-link').click(function(event) {
			event.preventDefault();

			if ($('.default').hasClass('active')) {
				$('.default').removeClass('active')
				$('.default').next('.default').addClass('active');
			}
		});*/


		// data-picker

		if ($('#datepicker').length > 0) {
			$( "#datepicker" ).datepicker({
		      changeMonth: true,
      		  changeYear: true,
      		  yearRange: "1980:2017"
		    });
		}




    //guillotine
    	if ($('#sample_picture').length>0) {
    		var picture = $('#sample_picture');
        //picture.on('load', function(){

        // Initialize plugin (with custom event)
        picture.guillotine({eventOnChange: 'guillotinechange'});

        // Display inital data
        var data = picture.guillotine('getData');

        for(var key in data) {
          $('#'+key).html(data[key]);
         }

        // Bind button actions
        $('#rotate_left').click(function(){
          picture.guillotine('rotateLeft');
        });

        $('#rotate_right').click(function(){
          picture.guillotine('rotateRight');
        });

        $('#fit').click(function(){
          picture.guillotine('fit');
        });

        $('#zoom_in').click(function(){
          picture.guillotine('zoomIn');
        });

        $('#zoom_out').click(function(){
          picture.guillotine('zoomOut');
        });

        // Update data on change
        picture.on('guillotinechange', function(ev, data, action) {
          data.scale = parseFloat(data.scale.toFixed(4));
          for(var k in data) { $('#'+k).html(data[k]); }
        });
    	}

      //});

      //slim scroll
      if ($('#chat-body').length>0) {
      	$('#chat-body').slimScroll({
	        height: '490px'
	    });
      }

      //textarea visibile

      $('#texar').click(function(event) {
      	event.preventDefault();
      	$('#text-vis').css('display','block');
      });

      //
      if ($('.star_holder').length>0) {
	      $('.star_holder').starbox({
	        average: 0.42,
	        changeable: 'once',
	        autoUpdateAverage: true,
	        ghosting: true
	      }).bind('starbox-value-changed', function(event, value) {
			console.log(event);
			console.log(value);
		});
	    }
	   //
	   $('#displ').click(function(event) {
	   	event.preventDefault();
	   	$('#adres').css('display','block');
	   });

	   //
	   $('.ui-datepicker-trigger').focus(function(event) {
	   	$('.ui-datepicker').css('display','block');
	   });
	   $('.ui-datepicker-trigger').focusout(function(event) {
	   	$('.ui-datepicker').css('display','none');
	   });

	   $( ".selector" ).datepicker( "setDate", "10/12/2012" );

	   //
	   $('.star-3').hover(function() {
	   	$('#par-chan').html('dfgdfgdg');
	   }, function() {
	   	$('#par-chan').html('ddfgdfgdfgggggggggg');
	   });

	   	$(document).on('mouseenter','.star-rating',function(e){
	   		count = $(this).index();
	   		inp = $(this).closest('.root_bl').find('input');

            //if($(inp.index()==count)){
            	//att = $(inp.index(count)).attr('data-text');
            	$.each(inp,function(i,el){
            		if((i+1)==count){
            			console.log($(el).attr('data-text'));
            			$('#par-chan').html($(el).attr('data-text'));
            		}
            	});
            //}
	   	});

	   	//
	   	$('#close').click(function(event) {
	   		$('.visitkaartje-wrapper').css('display','none');
	   	});

	   	//
	$(document).on('click','.profile-link,.profile-linkk,.messages,.messagess,.settings,.settingss,.betalen,.betalenn',function (e) {
        e.preventDefault();
        $('.tab-pane, .default').removeClass('active');
        var href = $(this).attr('href');
        $(href).addClass('active');
        var activetab = $(document).find('.tab-pane.active').attr('id');
        $('.default').each(function(indx, element){
			if(activetab==$(element).attr('data-tab')){
				$(element).addClass('active');
			}
        });
    });

	//
	$('.default a').on('click',function (e) {
		e.preventDefault();
    });

	//

	if($('.order-tabs_wrapper').length > 0){
        var url = window.location.href;
        var tabHash = url.split('#');
        $(document).find('[aria-controls="'+tabHash[1]+'"]').trigger('click');
        console.log($(document).find('[aria-controls="'+tabHash[1]+'"]'))
	}
	$(document).on('click','.welcome-container ul li > .li-content > a', function () {
		$(this).attr('href');
		location.reload();
    });

////////////////////////////////////////////////////////////////////////////////////

    $(document).on("click", "#cmd", function (e) {
        e.preventDefault();

        generate_pdf('.pdf-content');

        return false;
    });

    function generate_pdf(id){
        var el = $(id),
            cache_width = el.width(),
            a4 = [600.28,  841.89];  // for a4 size paper width and height

        $('body').scrollTop(0);
        $('#ulHide').hide();
        $('.upload-link').hide();
        el.addClass('to-pdf');


        el.width((a4[0] * 1.3333) - 80).css('max-width','none');

        console.log(el);


        var canvas2 = html2canvas(el,{
            imageTimeout:2000,
            removeContainer:true
        });

        //return false;

        canvas2.then(function(canvas){
            var img = canvas.toDataURL("image/jpeg");

            var doc = new jsPDF();
            $('#ulHide').show();
            $('.upload-link').show();
            $('.profile-order_wrapper .profile-order_container .left-container').removeClass('pdf-content');

            doc.addImage(img, 'JPEG' , 10 , 0);

            doc.save('document.pdf');
        });
    };

    if($('#kies1').prop( "checked" )){
        var kiesIn = $(this).find('.grey').html();
        console.log(kiesIn);
    }

    $('.input-logo').change(function(event){
    	var checked = $(this).prop('checked');
    	if(checked){

    		var imgSrc = $(this).parent().find('.logo-img').attr('src');

            $('#IMG').attr('src',imgSrc);

            console.log($(this).parent().find('.logo-img').attr('src'));
		}

	});

    $('#nonPaySyst').change(function(event){
    	var Checked = $(this).prop('checked');

        if(Checked){

            $('#hiddenLi').css('display','none');
        }
    });
    /*if($('.input-logo').prop( "checked" )){
        var paySyst = $(this).parent('radio-container').find('img').attr('src');
        console.log(paySyst);
    }*/

	/*if($('#input-logo').checked)*/

    $("#cr_form").change(function () {
    	var inputRadio = $(".cover-container").find('input:checked');
        var inputSib = inputRadio.siblings('.one-product_info').find('.bold').text();
        $('#cover').text(inputSib);

        var inputRadio2 = $(".kleuren-container").find('input:checked');
        var inputSib2 = inputRadio2.siblings('.one-product_info').find('.bold').text();
        $('#kleuren').text(inputSib2);

    });


    // add new products
    $(document).ready(function() {
        if (!$('.product-container').hasClass("hidden")){
            $('.show-more').css('display','none');
        }
        $(document).on('click','.show-more',function (e) {
            e.preventDefault();
            $('.product-container.hidden').each(function (index,element) {
                if(index<=3){
                    $(element).removeClass('hidden');
                }
                if (!$('.product-container').hasClass("hidden")){
                    $('.show-more').css('display','none');
                }
            });
        });
	});

	// media requires
    var window_size = window.matchMedia('(max-width: 768px)');
    if(window.matchMedia('(max-width: 768px)').matches)
    {
        $('.menu_to_open a').click(function(event) {
            event.preventDefault();
        });
        $('.menus h3').hide();
    }

});
