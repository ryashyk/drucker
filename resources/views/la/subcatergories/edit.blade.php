@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/subcatergories') }}">Subcatergory</a> :
@endsection
@section("contentheader_description", $subcatergory->$view_col)
@section("section", "Subcatergories")
@section("section_url", url(config('laraadmin.adminRoute') . '/subcatergories'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Subcatergories Edit : ".$subcatergory->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($subcatergory, ['route' => [config('laraadmin.adminRoute') . '.subcatergories.update', $subcatergory->id ], 'method'=>'PUT', 'id' => 'subcatergory-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'sub_name')
					@la_input($module, 'sub_link')
					@la_input($module, 'cat_name')
					@la_input($module, 'cat_id')
					@la_input($module, 'order')
					@la_input($module, 'status')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/subcatergories') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#subcatergory-edit-form").validate({
		
	});
});
</script>
@endpush
