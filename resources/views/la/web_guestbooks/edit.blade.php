@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/web_guestbooks') }}">Web guestbook</a> :
@endsection
@section("contentheader_description", $web_guestbook->$view_col)
@section("section", "Web guestbooks")
@section("section_url", url(config('laraadmin.adminRoute') . '/web_guestbooks'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Web guestbooks Edit : ".$web_guestbook->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($web_guestbook, ['route' => [config('laraadmin.adminRoute') . '.web_guestbooks.update', $web_guestbook->id ], 'method'=>'PUT', 'id' => 'web_guestbook-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'site_key')
					@la_input($module, 'username')
					@la_input($module, 'email')
					@la_input($module, 'title')
					@la_input($module, 'content')
					@la_input($module, 'add_time')
					@la_input($module, 'ip')
					@la_input($module, 're_content')
					@la_input($module, 're_username')
					@la_input($module, 're_time')
					@la_input($module, 'state')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/web_guestbooks') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#web_guestbook-edit-form").validate({
		
	});
});
</script>
@endpush
