@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/fromats') }}">Fromat</a> :
@endsection
@section("contentheader_description", $fromat->$view_col)
@section("section", "Fromats")
@section("section_url", url(config('laraadmin.adminRoute') . '/fromats'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Fromats Edit : ".$fromat->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($fromat, ['route' => [config('laraadmin.adminRoute') . '.fromats.update', $fromat->id ], 'method'=>'PUT', 'id' => 'fromat-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'info')
					@la_input($module, 'cover')
					@la_input($module, 'price_format')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/fromats') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#fromat-edit-form").validate({
		
	});
});
</script>
@endpush
