@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/the_printings') }}">The printing</a> :
@endsection
@section("contentheader_description", $the_printing->$view_col)
@section("section", "The printings")
@section("section_url", url(config('laraadmin.adminRoute') . '/the_printings'))
@section("sub_section", "Edit")

@section("htmlheader_title", "The printings Edit : ".$the_printing->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($the_printing, ['route' => [config('laraadmin.adminRoute') . '.the_printings.update', $the_printing->id ], 'method'=>'PUT', 'id' => 'the_printing-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'cover')
					@la_input($module, 'info')
					@la_input($module, 'price')
					@la_input($module, 'product_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/the_printings') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#the_printing-edit-form").validate({
		
	});
});
</script>
@endpush
