@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/why_chooses') }}">Why choose</a> :
@endsection
@section("contentheader_description", $why_choose->$view_col)
@section("section", "Why chooses")
@section("section_url", url(config('laraadmin.adminRoute') . '/why_chooses'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Why chooses Edit : ".$why_choose->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($why_choose, ['route' => [config('laraadmin.adminRoute') . '.why_chooses.update', $why_choose->id ], 'method'=>'PUT', 'id' => 'why_choose-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'text')
					@la_input($module, 'cover')
					@la_input($module, 'order_num')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/why_chooses') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#why_choose-edit-form").validate({
		
	});
});
</script>
@endpush
