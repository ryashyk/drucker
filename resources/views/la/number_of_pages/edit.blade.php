@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/number_of_pages') }}">Number of page</a> :
@endsection
@section("contentheader_description", $number_of_page->$view_col)
@section("section", "Number of pages")
@section("section_url", url(config('laraadmin.adminRoute') . '/number_of_pages'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Number of pages Edit : ".$number_of_page->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($number_of_page, ['route' => [config('laraadmin.adminRoute') . '.number_of_pages.update', $number_of_page->id ], 'method'=>'PUT', 'id' => 'number_of_page-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'as_product')
					@la_input($module, 'value')
					@la_input($module, 'price_numbers')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/number_of_pages') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#number_of_page-edit-form").validate({
		
	});
});
</script>
@endpush
