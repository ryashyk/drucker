@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/interest_rates') }}">Interest rate</a> :
@endsection
@section("contentheader_description", $interest_rate->$view_col)
@section("section", "Interest rates")
@section("section_url", url(config('laraadmin.adminRoute') . '/interest_rates'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Interest rates Edit : ".$interest_rate->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($interest_rate, ['route' => [config('laraadmin.adminRoute') . '.interest_rates.update', $interest_rate->id ], 'method'=>'PUT', 'id' => 'interest_rate-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'value')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/interest_rates') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#interest_rate-edit-form").validate({
		
	});
});
</script>
@endpush
