@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/user_coupons') }}">User coupon</a> :
@endsection
@section("contentheader_description", $user_coupon->$view_col)
@section("section", "User coupons")
@section("section_url", url(config('laraadmin.adminRoute') . '/user_coupons'))
@section("sub_section", "Edit")

@section("htmlheader_title", "User coupons Edit : ".$user_coupon->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($user_coupon, ['route' => [config('laraadmin.adminRoute') . '.user_coupons.update', $user_coupon->id ], 'method'=>'PUT', 'id' => 'user_coupon-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'user_id')
					@la_input($module, 'code')
					@la_input($module, 'date_set')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/user_coupons') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#user_coupon-edit-form").validate({
		
	});
});
</script>
@endpush
