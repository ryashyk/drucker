@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/product_weights') }}">Product weight</a> :
@endsection
@section("contentheader_description", $product_weight->$view_col)
@section("section", "Product weights")
@section("section_url", url(config('laraadmin.adminRoute') . '/product_weights'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Product weights Edit : ".$product_weight->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($product_weight, ['route' => [config('laraadmin.adminRoute') . '.product_weights.update', $product_weight->id ], 'method'=>'PUT', 'id' => 'product_weight-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'as_product')
					@la_input($module, 'value')
					@la_input($module, 'price_weigth')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/product_weights') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#product_weight-edit-form").validate({
		
	});
});
</script>
@endpush
