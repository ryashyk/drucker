@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/account_levels') }}">Account level</a> :
@endsection
@section("contentheader_description", $account_level->$view_col)
@section("section", "Account levels")
@section("section_url", url(config('laraadmin.adminRoute') . '/account_levels'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Account levels Edit : ".$account_level->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($account_level, ['route' => [config('laraadmin.adminRoute') . '.account_levels.update', $account_level->id ], 'method'=>'PUT', 'id' => 'account_level-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'level')
					@la_input($module, 'discount')
					@la_input($module, 'free_coupons')
					@la_input($module, 'turn_before')
					@la_input($module, 'turn_to')
					@la_input($module, 'cover')
					@la_input($module, 'order_levels')
					@la_input($module, 'active')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/account_levels') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#account_level-edit-form").validate({
		
	});
});
</script>
@endpush
