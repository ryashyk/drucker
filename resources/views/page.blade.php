@extends('main')

@section('content')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">{!! $page->name !!}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="veelgesteldevragen-wrapper">
        <div class="container veelgesteldevragen-container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
                    <div class="left-container">

                        {!! $page->text !!}


                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                    <div class="right-container">
                        @include('block.r_contact')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection