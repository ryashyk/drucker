@extends('user.index')



@section('content')


    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Contactgegevens</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="contactgegevens-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                    <div class="contact-left_container">
                        <h1>Contactgegevens</h1>
                        <ul class="bezoek-ul">
                            <li>
                                <h5>Bezoek/postadres</h5>
                            </li>
                            <li>
                                <span>MultiMedia Westland</span>
                            </li>
                            <li>
                                <span>Sand Ambachtstraat 136B</span>
                            </li>
                            <li>
                                <span>2691 BS  's-Gravenzande</span>
                            </li>
                        </ul>
                        <ul class="telefoon-ul">
                            <li>
                                <h5>Telefoon</h5>
                            </li>
                            <li>
                                <span>0174 -41 47 46</span>
                            </li>
                            <li>
                                <span>06-53 50 45 28</span>
                            </li>
                        </ul>
                        <ul class="bereikbaarheid-ul">
                            <li>
                                <h5>Bereikbaarheid</h5>
                            </li>
                            <li>
                                <span class="place">Maandag t/m donderdag</span>
                                <span class="data">8:30 tot 12.00 uur - 13:00 tot 17:00</span>
                            </li>
                            <li>
                                <span class="place">Vrijdag</span>
                                <span class="data">8:30 tot 12.00 uur</span>
                            </li>
                            <li>
                                <p>Na sluitingstijd altijd bereikbaar 24 uur per dag op 06-53504528</p>
                            </li>
                        </ul>
                        <ul class="email-ul">
                            <li>
                                <h5>Email adres</h5>
                            </li>
                            <li>
                                <a href="#" class="btn btn-primary btn-lg modalLink" data-toggle="modal" data-target="#myModal">info@multimediawestland.nl</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-offset-1">
                    <div class="contact-right_container">
                        <form>
                            <h2>Neem contact met ons op</h2>
                            <section class="form-section">
                                <label>Aanhef</label>
                                <div class="checkbox-container">
                                    <input type="radio" value="male" name="sex" checked="checked" id="radio" class="checkbox-input"/>
                                    <label for="radio" class="checkbox-label">Dhr.</label>
                                </div>
                                <div class="checkbox-container">
                                    <input type="radio" value="female" name="sex" id="radio-1" class="checkbox-input"/>
                                    <label for="radio-1" class="checkbox-label">Mevr.</label>
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Naam</label>
                                <div class="input-container">
                                    <input type="text" name="first_name" required="required" placeholder="Voornaam" >
                                    <input type="text" name="last_name" required="required" placeholder="Achternaam" >
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Telefoonnummer</label>
                                <input type="text" name="phone" required="required" placeholder="Bijv. +31 11 22 33" >
                            </section>
                            <section class="form-section">
                                <label>E-mailadres</label>
                                <input type="text" name="email" required="required">
                            </section>
                            <section class="form-section">
                                <label>Jouw vraag</label>
                                <textarea name="text" required="required"></textarea>
                            </section>
                            <section class="form-section_button">
                                <button type="submit">verzenden</button>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="map"  class="global-map_wrapper">
        </div>



        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <span>verzonden bericht</span>
                    </div>
                </div>
            </div>
        </div>

<!--div class="contact-us _height_l">

    <div class="container container-sm">

        <div class="col-lg-12 __contact-us-inner">

            <h2>Contact Us</h2>

            <span id="message"></span>

            <form id="contact_form" data-action="/ajax" autocomplete="off">

                <input type="email" name="email" placeholder="Your Email">

                <input type="hidden" name="action" value="contact_us">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <textarea placeholder="Your Message" name="text"></textarea>

                <button type="submit" class="_size_s">Send</button>

            </form>

        </div>

    </div>

</div-->
</div>
@endsection