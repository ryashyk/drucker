@extends('main')

@section('content')

    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="{{ $category->link }}">{{ $category->name }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="products-wrapper">
        <div class="container profile-mijn_container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="left-container">
                        <h1>{{ $category->name }}</h1>
                        <div class="row product-row">
                            @if(count($products)>0)
                                @php $class = ''; $i=0; @endphp
                                <div class="row product-row">
                                    @foreach($products as $k)
                                        @php $i = $i + 1 ;@endphp
                                        @if($i>=13)
                                            @php $class = 'hidden'; @endphp
                                        @endif
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 product-container {{ $class }}">
                                            <a href="/product-one/id-{{ $k->id }}" class="bekijk">Bekijk nu</a>
                                            <a href="/product-one/id-{{ $k->id }}" class="product-link">
                                                <div class="product-img">
                                                    <img src="/storage/uploads{{$k->path}}" alt="">
                                                </div>
                                                <div class="product-text">
                                                    <span>{{ $k->name }}</span>
                                                </div>
                                            </a>

                                        </div>
                                    @endforeach
                                </div>
                            @endif

                        </div>
                        <button class="show-more">toon meer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection