@extends('main')

@section('content')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Zoek een product</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="products-wrapper">
        <div class="container profile-mijn_container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="left-container">
                        <h1>Zoek een product - {{ $name }}</h1>
                        @if(count($products)>0)
                        <div class="row product-row">
                                @foreach($products as $k)
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 product-container">
                                        <a href="/product-one/id-{{ $k->id }}" class="bekijk">Bekijk nu</a>
                                        <a href="/product-one/id-{{ $k->id }}" class="product-link">
                                            <div class="product-img">
                                                <img src="/public/uploads/product/{{$k->photo_name}}" alt="">
                                            </div>
                                            <div class="product-text">
                                                <span>{{ $k->name }}</span>
                                            </div>
                                        </a>

                                    </div>
                               @endforeach
                        </div>
                        @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>



@endsection

