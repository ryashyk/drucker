<div class="modal fade removed-modal js-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <h3>Delivery Information</h3>
                <div class="form-wrap">
                    <div class="__row">
                        <label for="character-name">Character Name:</label>
                        <input type="text" id="character-name">
                    </div>
                    <div class="__row">
                        <label for="deliver-method">Deliver Method:</label>
                        <div class="__select-wrap">
                            <select class="js-select" id="deliver-method">
                                <option value="Face to Face">Face to Face</option>
                                <option value="Auction House">Auction House</option>
                                <option value="Guild Bank">Guild Bank</option>
                            </select>
                        </div>
                    </div>
                    <div class="__row">
                        <label for="modal-message">Message:</label>
                        <textarea id="modal-message"></textarea>
                    </div>
                    <div class="__row">
                        <button type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>