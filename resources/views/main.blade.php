<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> {{ $title }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/simple-line-icons.css')}}">

    <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/jquery.arcticmodal-0.3.css')}}">

    <link rel="stylesheet" href="{{asset('/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('/css/lightbox.css')}}">
    <link rel="stylesheet" href="{{asset('/css/select2.min.css')}}">
    <!-- Select -->
    <link rel="stylesheet" href="{{asset('/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('/css/jquery.guillotine.css')}}">

    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">

</head>

<body>
<header class="header" id="header">
    <div class="contact_info-wrapper">
        <div class="container">
            <div class="contact_info">
                <p class="icon-phone"><span>0174 - 414746</span></p>
                <p class="icon-envelope"><span>info@multimediawestland.nl </span></p>
                <p class="icon-earphones-alt"><span>We zijn nu open!</span></p>
            </div>
        </div>
    </div>
    <div class="elements_header-wrapper">
        <div class="container">
            <div class="elements_header">
                <div class="logo">
                    <a href="/">
                        <img src="/img/logo.jpg" alt="">
                    </a>
                </div>
                <div class="work-time_wrapper">
                    <h4>Openingstijden</h4>
                    <i class="fa fa-clock-o fa-3x" aria-hidden="true"></i>
                    <ul>
                        <li>
                            <span class="left">Ma. t/m don.</span>
                            <span class="right">08:00 -12:00 / 13:00-17:00 uur</span>
                        </li>
                        <li>
                            <span class="left">Vrijdag</span>
                            <span class="right">08:00-12:00 uur</span>
                        </li>
                        <li>
                            <span class="left">Zaterdag</span>
                            <span class="right">gesloten</span>
                        </li>
                    </ul>
                </div>
                <div class="elements">
                    <div class="hotline">
                        <img src="/img/callback-girl.jpg" alt="">
                        <span>HOTLINE NUMBER</span>
                        <a href="tel:0174414746">0174 - 414746</a>
                    </div>
                    <ul>
                        <li><a class="login" href="/login_user" data-i="profile_user">
                                <img src="/img/user-icon.png" alt="">
                            </a></li>
                        <li><a class="icon-basket-loaded" href="/cart">
                                <span>{{ $cart_items }}</span>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header_menu-wrapper">
        <div class="container">
            <div class="header_menu">
                <div class="row">
                    <div class="menu col-lg-10 col-md-11 col-xs-12">
                        <button class="open_menu fa fa-bars"></button>
                        <div class="frame_bl">
                            <ul class="slidee">
                                <li><a class="icon-home" href="/"></a></li>
                                @if(count($home_menu)>0)
                                    @foreach($home_menu as $k)
                                <li class="menu_to_open" data-id="{{ $k->id }}">
                                    <a href="{{ $k->link }}">{{ $k->name }}</a>
                                </li>
                                    @endforeach
                               @endif
                            </ul>

                        </div>
                        <div class="scrollbar">
                            <div class="handle"><div class="mousearea"></div></div>
                        </div>

                        <div class="hidden_bl">
                            @if(count($home_menu)>0)
                                @foreach($home_menu as $k)
                                        <div class="hidden_menu" data-hidden-id="{{ $k->id }}">
                                            <div class="container">
                                                <div class="opened_menu">
                                                    <div class="row">
                                                        <div class="menus col-lg-9 col-md-9 col-xs-12">
                                                            <h3>{{ $k->name }}</h3>
                                                            <ul>
                                                                @if(count($k->sub)>0)
                                                                    @foreach($k->sub as $v)
                                                                        <li><a href="{{ $v->sub_link }}">{{ $v->sub_name }}</a></li>
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                        </div>
                                                        <div class="img_menu-wrapper col-lg-3 col-md-3 col-xs-12">
                                                            <img src="/img/image.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="search col-lg-2 col-md-1 col-xs-12">
                        <button class="open_search icon-magnifier"></button>
                    </div>
                    <form id="search_form" action="/search" method="POST" class="search_form">
                        <label>
                            <input type="text" name="name" placeholder="Zoek een product...">
                            {!!   csrf_field() !!}
                            <button class="icon-magnifier"></button>
                        </label>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>

<main class="main" id="main">

    @yield('content')
    <div class="links-wrapper">
        <div class="container">
            <div class="links">
                <div class="row flex-bl">
                    @if(count($home_menu)>0)
                        @foreach($home_menu as $k)
                            <div class="col-lg-2 col-md-2 col-xs-12">
                                <h4>{{ $k->name }}</h4>
                                <ul>
                                    @if(count($k->sub)>0)
                                        @foreach($k->sub as $v)
                                            <li><a href="{{ $v->sub_link }}">{{ $v->sub_name }}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        @endforeach

                    @endif

                </div>
            </div>
        </div>
    </div>
    </div>
</main>

<footer class="footer" id="footer">
    <div class="foot_wrapper">
        <div class="container">
            <div class="foot">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-xs-12">
                        <div class="logo_foot">
                            <a href="">
                                <img src="/img/logo_footer.png" alt="">
                            </a>
                        </div>
                        <ul class="payment_system">
                            <li><img src="/img/idea-logo.png" alt=""></li>
                            <li><img src="/img/paypal-logo.png" alt=""></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <h4>MultiMedia Westland</h4>
                        <ul class="foot_menu">
                            <li><a href="/page/aanleverspecificaties">Aanleverspecificaties</a></li>
                            <li><a href="/page/werktekeningen">Werktekeningen</a></li>
                            <li><a href="/faq">Veel gestelde vragen</a></li>
                            <li><a href="/contact">Contactgegevens</a></li>
                            <li><a href="/page/voorwaarden">Algemene voorwaarden</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <h4>Contactgegevens</h4>
                        <ul class="contacts_list">
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i> Sand Ambachtstraat 136B 2691 BS's-Gravenzande</li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i> 0174 -41 47 46 </li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i> 06-53 50 45 28</li>
                            <li><a href="mailto:info@multimediawestland.nl"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@multimediawestland.nl</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-2 col-xs-12">
                        <h4>Openingstijden</h4>
                        <div class="social-time_container">
                            <ul class="work-time_container">
                                <li>
                                    <span class="left">Ma. t/m don.</span>
                                    <span class="right">08:00 -12:00 / 13:00-17:00 uur</span>
                                </li>
                                <li>
                                    <span class="left">Vrijdag</span>
                                    <span class="right">08:00-12:00 uur</span>
                                </li>
                                <li>
                                    <span class="left">Zaterdag</span>
                                    <span class="right">gesloten</span>
                                </li>
                            </ul>
                            <ul class="social">
                                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>




<script src="{{asset('/js/jquery-3.1.1.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcoPU5bL4ZG-kNgZkAA7IqCynhq5nLCbs&signed_in=true&libraries=places&language=en"></script>

<script src="{{asset('/js/bootstrap.min.js')}}"></script>

<script src="{{asset('/js/slick.min.js')}}"></script>

<script src="{{asset('/js/jquery.arcticmodal-0.3.min.js')}}"></script>
<script src="{{asset('/js/select2.min.js')}}"></script>
<script src="{{asset('/js/lightbox.js')}}"></script>
<script src="{{asset('/js/jquery.guillotine.js')}}"></script>
<script src="{{asset('/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('/js/jquery-ui.js')}}"></script>
<script src="{{asset('/js/jspdf.min.js')}}"></script>
<script src="{{asset('/js/html2canvas.js')}}"></script>
<script src="{{asset('/js/sly.js')}}"></script>
<script src="{{asset('/js/anonym.js')}}"></script>

<script src="{{asset('/js/art.js')}}"></script>

</body>

</html>