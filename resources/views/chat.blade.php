@extends('main')

@section('content')

    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="/profile_user">Mijn account</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Chat</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="chat-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                    <div class="left-container">
                        <h1>Chat</h1>
                        <div class="chat-container">
                            <div class="header">
                                <h3>Chat support</h3>
                            </div>
                            <div id="chat-body" class="main">
                                <!--span class="date">09 april 2017</span>
                                <div class="text-area">
                                    <!--div class="message">
                                        <span class="time">10:05</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">Hello!</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:07</span>
                                        <div class="account-img">
                                            <img src="/img/chat-2.png">
                                        </div>
                                        <div class="text-line">Hi there! What can I help you with today?</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:08</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">My order number is 10056564. I&prime;m wonderign if it&prime;s too late to modify that order?</div>
                                    </div>
                                </div>
                                <span class="date">09 april 2017</span>
                                <div class="text-area">
                                    <div class="message">
                                        <span class="time">10:05</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">Hello!</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:07</span>
                                        <div class="account-img">
                                            <img src="/img/chat-2.png">
                                        </div>
                                        <div class="text-line">Hi there! What can I help you with today?</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:08</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">My order number is 10056564. I&prime;m wonderign if it&prime;s too late to modify that order?</div>
                                    </div>
                                </div>
                                <span class="date">09 april 2017</span>
                                <div class="text-area">
                                    <div class="message">
                                        <span class="time">10:05</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">Hello!</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:07</span>
                                        <div class="account-img">
                                            <img src="/img/chat-2.png">
                                        </div>
                                        <div class="text-line">Hi there! What can I help you with today?</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:08</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">My order number is 10056564. I&prime;m wonderign if it&prime;s too late to modify that order?</div>
                                    </div>
                                </div>
                                <span class="date">09 april 2017</span>
                                <div class="text-area">
                                    <div class="message">
                                        <span class="time">10:05</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">Hello!</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:07</span>
                                        <div class="account-img">
                                            <img src="/img/chat-2.png">
                                        </div>
                                        <div class="text-line">Hi there! What can I help you with today?</div>
                                    </div>
                                    <div class="message">
                                        <span class="time">10:08</span>
                                        <div class="account-img">
                                            <img src="/img/user-icon.png">
                                        </div>
                                        <div class="text-line">My order number is 10056564. I&prime;m wonderign if it&prime;s too late to modify that order?</div>
                                    </div>
                                </div-->
                            </div>
                            <div class="footer">
                                <div class="textarea-container">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Write the message"></textarea>
                                    <div class="account-img">
                                        <img src="/img/user-icon.png">
                                    </div>
                                    <div class="under-textarea">
                                        <button type="submit">sturen</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <div class="right-container">
                        @include('block.top_user_info')
                        @include('block.r_profile_menu')
                        @include('block.r_contact')
                    </div>
                </div>
            </div>
        </div>

@endsection