@extends('main')



@section('content')

    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Winkelwagen</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @if( Cart::total()<=0)
        <div class="cart-wrapper">
            <div class="container profile-mijn_container">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 id="tabtext">Uw winkelwagen is leeg</h1>
                        <div class="basket-wrapper"></div>
                    </div>
                </div>
            </div>


     @else
    <div class="cart-wrapper">
        <div class="container profile-mijn_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 id="tabtext">Winkelwagen</h1>
                <div class="tabs-wrapper">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active default" data-tab="home">
                            <a href="prevent-link" >
                                <i class="fa fa-shopping-cart icon-flav" aria-hidden="true"></i>
                                WINKELWAGEN
                            </a>
                        </li>
                        <li role="presentation" class="default" data-tab="profile">
                            <a href="prevent-link" >
                                <i class="fa fa-map-marker  icon-flav" aria-hidden="true"></i>NAAR                  Aflevermethode</a>
                        </li>
                        <li role="presentation" class="default" data-tab="messages"><a href="prevent-link"><i class="fa fa-credit-card-alt icon-flav" aria-hidden="true"></i>Betalingswijze</a></li>
                        <li role="presentation" class="default" data-tab="settings"><a href="prevent-link" ><i class="fa fa-barcode icon-flav" aria-hidden="true"></i>Prijsopgave</a></li>
                        <li role="presentation" class="default"
                            data-tab="betalen"><a href="prevent-link" ><i class="fa fa-check-circle-o icon-flav" aria-hidden="true"></i>Aanleveren en betalen</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                <div class="left-container">
                    <form id="cart_orders">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="tab-1_wrapper">
                                <ul class="ul-header">
                                    <li>
                                        <span class="left">Brochures / Magazines (geniet)</span>
                                        <span class="top">&euro; {{ Cart::total() }}</span>
                                        <span class="down">excl. btw</span>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="">
                                            <span>Leveren voor 11 uur&rsquo;s ochtends (&euro; 0)</span>
                                        </label>
                                        <span class="right">Order {{ 'ID'.time() }}</span>
                                        <input type="hidden" name="order_id" value="{{'ID'.time()}}">
                                        <input type="hidden" name="action" value="add_order">
                                        {{ csrf_field() }}
                                    </li>
                                </ul>
                                <ul class="ul-main">
                                    <li>
                                        <span class="left">Subtotaal</span>
                                        <span class="right">&euro; {{ Cart::total() }}</span>
                                    </li>
                                    <li>
                                        <span class="left">Verzendkosten</span>
                                        <span class="right">&euro; 0,00</span>
                                    </li>
                                    <li>
                                        <span class="left">BTW 21%</span>
                                        <span class="right">&euro; {{ (round((Cart::total()*0.21),2)) }}</span>
                                    </li>
                                    <li>
                                        <span class="left">Totaal</span>
                                        <span class="right">&euro; {{ ((Cart::total())+(round((Cart::total()*0.21),2))) }}</span>
                                    </li>
                                </ul>
                                <div class="link-container">
                                    <a href="#profile" id="transitor-link" aria-controls="profile" role="tab" data-toggle="tab" class="next profile-link" onClick="document.getElementById('tabtext').innerHTML='Aflever en factuur adres';">NAAR Aflevermethode<i class="fa fa-angle-right fa-1x" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="tab-2_wrapper">
                                <div class="top-wrapper">
                                    <div class="left-container">
                                        <span>Factuuradres</span>
                                        <p>Let op: wij versturen je factuur standaard per e-mail</p>
                                        <div class="radio-wrapper-standart">
                                            <div class="radio-container">
                                                <label>
                                                    Standaard
                                                    <input type="radio" name="standart" value="Standaard">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-container">
                                        <ul>
                                            <li>
                                                <input type="hidden" name="delivery" value="Standart">
                                                <span>Factuuradres</span>
                                            </li>
                                            <li>
                                                <span>MultiMedia Westland</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="down-wrapper">
                                    <div class="left-container">
                                        <span class="bold">Aflevermethode</span>
                                        <div class="radio-wrapper">
                                            <div class="radio-container">
                                                <span>Afleveren</span>
                                                <label>
                                                    Standaardlevering met DHL
                                                    <input type="radio" name="delivery" value="Standaardlevering met DHL">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="radio-container">
                                                <span>Afhalen</span>
                                                <label>
                                                    Afhalen &#8242;s-Gravenzande
                                                    <input type="radio" name="delivery" value="Afhalen s-Gravenzande">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-container">
                                        <ul>
                                            <li>
                                                <span>Afleveradres</span>
                                            </li>
                                            <li>
                                                <span>MultiMedia Westland</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="link-container">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="prev profile-linkk" onClick="document.getElementById('tabtext').innerHTML='Winkelwagen';"><i class="fa fa-angle-left" aria-hidden="true"></i>winkelwagen</a>
                                    <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" id="transitor-link" class="next messages" onClick="document.getElementById('tabtext').innerHTML='Betalingswijze';">NAAR Aflevermethode<i class="fa fa-angle-right fa-1x" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages">
                            <div class="tab-3_wrapper">

                                <div class="main">
                                    <h3>Betalingswijze</h3>
                                    <div class="radio-wrapper">
                                        <div class="radio-container">
                                            <label>
                                                <p>iDeal<span>(Gratis)</span></p>
                                                <input type="radio" name="pay_type" value="ideal" class="input-logo" checked="checked">
                                                <div class="circle-input">
                                                    <span class="grey-circle"></span>
                                                </div>
                                                <img src="/img/pay.png" class="money-syst_anon logo-img">
                                            </label>
                                        </div>
                                        <div class="radio-container">
                                            <label>
                                                <p>PayPal<span>(Gratis)</span></p>
                                                <input type="radio" name="pay_type" value="paypal" class="input-logo">
                                                <div class="circle-input">
                                                    <span class="grey-circle"></span>
                                                </div>
                                                <img src="/img/pal.png" class="money-syst_pay logo-img">
                                            </label>
                                        </div>
                                        <div class="radio-container">
                                            <label>
                                                <p>Op rekening<span>(in overleg)</span></p>
                                                <input type="radio" name="pay_type" id="nonPaySyst">
                                                <div class="circle-input">
                                                    <span class="grey-circle" id="logo-img"></span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!--div class="footer">
                                    <span>Actiecod</span>
                                    <a href="">+ toevoegen</a>
                                </div-->
                                <div class="link-container">
                                    <a href="#profile" aria-controls="messages" role="tab" data-toggle="tab" class="prev messagess" onClick="document.getElementById('tabtext').innerHTML='Aflever en factuur adres';"><i class="fa fa-angle-left" aria-hidden="true"></i>winkelwagen</a>
                                    <a href="#settings" aria-controls="home" role="tab" data-toggle="tab" id="transitor-link" class="next settings" onClick="document.getElementById('tabtext').innerHTML='Prijsopgave';">NAAR Aflevermethode<i class="fa fa-angle-right fa-1x" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="tab-4_wrapper">
                                <ul class="ul-header">
                                    <li>
                                        <span class="left">Brochures / Magazines (geniet)</span>
                                        <span class="top">&euro; 0</span>
                                    </li>
                                </ul>
                                <ul class="ul-main">
                                    <li>
                                        <span class="left">Subtotaal</span>
                                        <span class="right">&euro; 0</span>
                                    </li>
                                    <li>
                                        <span class="left">Betalingswijze (iDeal)</span>
                                        <span class="right">&euro; 0,00</span>
                                    </li>
                                    <li>
                                        <span class="left">Verzendkosten</span>
                                        <span class="right">GRATIS</span>
                                    </li>
                                    <li>
                                        <span class="left">Klantenkorting</span>
                                        <span class="right">&euro; 0,00</span>
                                    </li>
                                    <li>
                                        <span class="left">Totaal excl. BTW</span>
                                        <span class="right">&euro; {{ Cart::total() }}</span>
                                    </li>
                                    <li>
                                        <span class="left">BTW 21%</span>
                                        <span class="right">&euro; {{ (round((Cart::total()*0.21),2)) }}</span>
                                    </li>
                                    <li>
                                        <span class="left">Totaal</span>
                                        <span class="right">&euro; {{ ((Cart::total())+(round((Cart::total()*0.21),2))) }}</span>
                                    </li>
                                </ul>
                                <div class="link-container">
                                    <a href="#messages" aria-controls="home" role="tab" data-toggle="tab" class="prev settingss" onClick="document.getElementById('tabtext').innerHTML='Betalingswijze';"><i class="fa fa-angle-left" aria-hidden="true"></i>winkelwagen</a>
                                    <a href="#betalen" aria-controls="home" role="tab" data-toggle="tab" id="transitor-link" class="next betalen" onClick="document.getElementById('tabtext').innerHTML='Aanleveren en betalen';">NAAR Aflevermethode<i class="fa fa-angle-right fa-1x" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="betalen">
                            <div class="tab-5_wrapper">
                                <div class="header">
                                    <ul>
                                        <li>Je bent zowat klaar</li>
                                        <li>De besteling is geoplaats uw order number  is 100032656</li>
                                        <li>
                                            U heeft een bevestigings mail ontvangen op
                                            <a href="mailto:info@multimediawestland.nl">info@multimediawestland.nl</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="main">
                                    <ul>
                                        <li>Nu betalen</li>
                                        <li>Na betaling nemen we je order direct in behandeling.</li>
                                        <li id="hiddenLi"><img src="/img/pay.png" id="IMG">Betaal met iDeal</li>
                                        <li><button type="submit">BETALEN &euro; {{ ((Cart::total())+(round((Cart::total()*0.21),2))) }} incl. BTW</button></li>


                                    </ul>
                                </div>
                                <div class="link-container">
                                    <a href="#settings" aria-controls="home" role="tab" data-toggle="tab" class="prev betalenn" id="transitor-link" onClick="document.getElementById('tabtext').innerHTML='Prijsopgave';"><i class="fa fa-angle-left" aria-hidden="true"></i>winkelwagen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>

                <form id="paypal" style="display: none;" method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr" name="paypal_auto_form"/>

                <input type="hidden" name="business" value="info-facilitator@rif-property.fr" />
                <input type="hidden" name="rm" value="2" />
                <input type="hidden" name="cmd" value="_xclick" />
                <input type="hidden" name="currency_code" value="EUR" />
                <input type="hidden" name="quantity" value="1" />
                <input type="hidden" name="return" value="http://drucker.isolly.com/profile_user" />
                <input type="hidden" name="cancel_return" value="http://drucker.isolly.com/profile_user" />
                <input type="hidden" name="notify_url" value="http://drucker.isolly.com/profile_user" />
                <input type="hidden" name="item_name" value="Order {{ 'ID'.time() }}" />
                <input type="hidden" name="custom" value="1" />
                <input type="hidden" name="item_number" value="{{ time() }}" />
                <input type="hidden" name="amount" value="{{ ((Cart::total())+(round((Cart::total()*0.21),2))) }}" />

                <button type="submit"  name="pp_submit">book</button>
                <a href="javascript:history.back();">Change reservation settings</a>
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                <div class="right-container">
                    @include('block.r_contact')
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection