@extends('main')

@section('content')

    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="#">Promotie</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">{{ $product_data->name }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="one-product_wrapper">
        <div class="container profile-mijn_container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                    <div class="left-container">
                        <h1>{{ $product_data->name }}</h1>
                        <div class="header-product">
                            <div class="product-image">
                                <a href="/storage/uploads{{$product_data->path}}" data-lightbox="image-1" data-title="{{ $product_data->name }}">
                                    <img src="/storage/uploads{{$product_data->path}}" alt="{{ $product_data->name }}">
                                </a>
                            </div>
                            <div class="tabs-wrapper">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#informatie" aria-controls="home" role="tab" data-toggle="tab">Informatie</a></li>
                                    <li role="presentation"><a href="#aanleverspecificaties" aria-controls="profile" role="tab" data-toggle="tab">Aanleverspecificaties</a></li>
                                    <li role="presentation"><a href="#werktekeningen" aria-controls="messages" role="tab" data-toggle="tab">Werktekeningen</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="informatie">
                                        <div class="tab-1_wrapper">
                                            <p>{!! $product_data->info !!}</p>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="aanleverspecificaties">
                                        <div class="tab-2_wrapper">
                                            <p>{!! $product_data->delivery !!}</p>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="werktekeningen">
                                        <div class="tab-3_wrapper">
                                            <p>{!! $product_data->drawings !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-product">

                            <form id="cr_form" autocomplete="false">
                                @if(count($product_data->colors)>0)
                                    <div class="kleuren-container">
                                        <h3>Kleuren</h3>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                                <div class="row">
                                                    <ul>
                                                        @foreach($product_data->colors as $k=>$v)
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="color" {{ ($k==0) ? 'checked="checked"' : '' }} value="{{ $v->name }} - {{ $v->hex }}">
                                                                    <span class="arrow-back"></span>
                                                                    <div class="one-product_info">

                                                                        <span class="color" style="background:{{ $v->hex }};"></span>

                                                                        <section>
                                                                            <span class="bold">{{ $v->name }}</span>
                                                                        </section>
                                                                    </div>
                                                                </label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif



                                <input type="hidden" name="action" value="add_to_cart">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$product_data->id }}">
                                    @if(count($product_data->formats)>0)
                                        <div class="kies-container">
                                            <h3>Kies je formaat</h3>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                                    <div class="row">

                                                            @foreach($product_data->formats as $k=>$v)
                                                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                                                    <label>
                                                                        <input type="radio" name="formats" id="kies1" value="{{ $v->id }}" {{ ($k==0) ? 'checked="checked"' : '' }}>
                                                                        <span class="arrow-back"></span>
                                                                        <div class="one-product_info">
                                                                            <figure>
                                                                                <img src="/storage/uploads{{$v->path}}">
                                                                            </figure>
                                                                            <section>
                                                                                <span class="bold">{{ $v->name }}</span>
                                                                                <span class="grey">{{ $v->info }}</span>
                                                                            </section>
                                                                        </div>
                                                                    </label>
                                                                </div>
                                                            @endforeach


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @if(count($product_data->printings)>0)
                                <div class="bedrukking-container">
                                    <h3>Kies de bedrukking</h3>
                                    <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                    <div class="row">



                                            @foreach($product_data->printings as $k=>$v)
                                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                                    <label>
                                                        <input type="radio" name="printings" id="kies2" value="{{ $v->id }}" {{ ($k==0) ? 'checked="checked"' : '' }}>
                                                        <span class="arrow-back"></span>
                                                        <div class="one-product_color">
                                                            <figure>
                                                                <img src="/storage/uploads{{$v->path}}">
                                                            </figure>
                                                            <section>
                                                                <span class="bold">{{ $v->cover }}</span>
                                                                <span class="grey">{{ $v->info }}</span>
                                                            </section>
                                                        </div>
                                                    </label>
                                                </div>
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if(count($product_data->materials)>0)
                                <div class="materiaal-container">
                                    <h3>Kies het materiaal</h3>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">

                                                @foreach($product_data->materials as $k=>$v)
                                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                                    <label>
                                                            <input type="radio" id="kies3" name="materials" value="{{ $v->id }}" {{ ($k==0) ? 'checked="checked"' : '' }}>
                                                            <span class="arrow-back"></span>
                                                            <div class="one-product_materiaal">
                                                                <figure>
                                                                    <img src="/storage/uploads{{$v->path}}">
                                                                </figure>
                                                                <section>
                                                                    <span class="bold">{{ $v->name }}</span>
                                                                </section>
                                                            </div>
                                                        </label>
                                                    </div>
                                                @endforeach

                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!--div class="gewicht-container">
                                    <h3>Kies het gewicht</h3>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" id="kies4" name="action-4" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">115 g/m²</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-4" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">170 g/m²</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-4" value="1" checked="true">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">135 g/m²</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-4" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">250 g/m²</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div-->
                                <!--div class="aantal-container">
                                    <h3>Kies het aantal pagina's (incl. omslag)</h3>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">8 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">44 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-5" value="1" checked="true">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">12 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">48 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">16 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">52 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-5" value="1" checked="true">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">20 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">56 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-5" value="1" checked="true">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">24 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">60 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">28 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">64 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4">
                                                <label>
                                                    <input type="radio" name="action-5" value="1">
                                                    <div class="circle-input">
                                                        <span class="grey-circle"></span>
                                                    </div>
                                                    <span class="arrow-back"></span>
                                                    <div class="one-product_materiaal">
                                                        <section>
                                                            <span class="bold">32 pagina&prime;s</span>
                                                        </section>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div-->
                                <div class="cover-container">
                                    <h3>Kies de cover / omslag</h3>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                                    <label>
                                                        <input type="radio" name="action-6" value="1" checked="true">
                                                        <span class="arrow-back"></span>
                                                        <div class="one-product_info">
                                                            <figure>
                                                                <img src="/img/prod-11.png">
                                                            </figure>
                                                            <section>
                                                                <span class="bold">Selfcover</span>
                                                            </section>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                                    <label>
                                                        <input type="radio" name="action-6" value="2">
                                                        <span class="arrow-back"></span>
                                                        <div class="one-product_info">
                                                            <figure>
                                                                <img src="/img/prod-12.png">
                                                            </figure>
                                                            <section>
                                                                <span class="bold">170 grams glans</span>
                                                            </section>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                                    <label>
                                                        <input type="radio" name="action-6" value="3">
                                                        <span class="arrow-back"></span>
                                                        <div class="one-product_info">
                                                            <figure>
                                                                <img src="/img/prod-13.png">
                                                            </figure>
                                                            <section>
                                                                <span class="bold">170 grams mat</span>
                                                            </section>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                                    <label>
                                                        <input type="radio" name="action-6" value="4">
                                                        <span class="arrow-back"></span>
                                                        <div class="one-product_info">
                                                            <figure>
                                                                <img src="/img/prod-14.png">
                                                            </figure>
                                                            <section>
                                                                <span class="bold">250 grams glans</span>
                                                            </section>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                                    <label>
                                                        <input type="radio" name="action-6" value="5">
                                                        <span class="arrow-back"></span>
                                                        <div class="one-product_info">
                                                            <figure>
                                                                <img src="/img/prod-15.png">
                                                            </figure>
                                                            <section>
                                                                <span class="bold">250 grams mat</span>
                                                            </section>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="levering-container">
                                    <!--h3>Kies je oplage en levering</h3>
                                    <div class="left-table">
                                        <span class="bold">Full color digitaal</span>
                                        <span class="grey">Goedkoper en sneller bij kleine oplages</span>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="oplage">Oplage</th>
                                                    <th class="levering">Normale levering<span>3 werkdagen</span></th>
                                                    <th class="next">Next-day<span>2 werkdagen</span></th>
                                                </tr>
                                                <tr>

                                                    <td class="oplage">1
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label>
                                                    </td>
                                                    <td class="levering">&#8364; 19.49
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 31,99
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">2
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 22,23
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 34,73
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">3
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 24,97
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 37,47
                                                        <label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">4<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 27,71<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 40,21<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">5<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 30,45<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 42,95<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">10<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 44,14<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 56,64<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">15<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 53,35<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 65,85<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">25<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 78,25<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next">&#8364; 90,75<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">50<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 140,50<label>
                                                            <input type="radio" name="action-7" value="" checked="true">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next"></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">100<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 247,90<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next"></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">200<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 488,80<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next"></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">250<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 584.00<label>
                                                            <input type="radio" name="action-7" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="next"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="right-table">
                                        <span class="bold">Full color offset</span>
                                        <span class="grey">Voordeliger bij grote oplages</span>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="oplage">Oplage</th>
                                                    <th class="levering">Normale levering<span>5 werkdagen</span></th>
                                                </tr>
                                                <tr>

                                                    <td class="oplage">500
                                                        <label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label>
                                                    </td>
                                                    <td class="levering">&#8364; 477,15
                                                        <label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">1000
                                                        <label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 611,90
                                                        <label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">2000
                                                        <label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 907,48
                                                        <label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">3000<label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 1.317,48<label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">4000<label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 1.499,42<label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                                <tr>
                                                    <td class="oplage">5000<label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                    <td class="levering">&#8364; 1.571,74<label>
                                                            <input type="radio" name="action-8" value="">
                                                            <span class="arrow-back"></span>
                                                            <span class="border"></span>
                                                        </label></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div-->
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <div class="right-container">

                        @include('block.r_contact')
                        <aside class="overzicht-container">
                            <span>Overzicht</span>
                            <ul class="information-ul">
                                @if(count($product_data->formats))
                                    <li>
                                        <span class="bold">Kies het eindformaat</span>
                                        <span class="grey">{{$product_data->formats[0]->name}} {{$product_data->formats[0]->info}} </span>
                                    </li>
                                @endif

                                    @if(count($product_data->colors))
                                        <li>
                                            <span class="bold">Kleuren</span>
                                            <span class="grey" id="kleuren">{{$product_data->colors[0]->name}}</span>
                                        </li>
                                    @endif
                                @if(count($product_data->printings))
                                        <li>
                                            <span class="bold">Kies de bedrukking</span>
                                            <span class="grey">{{$product_data->printings[0]->cover}} {{$product_data->printings[0]->info}} </span>
                                        </li>
                                @endif
                                @if(count($product_data->materials))
                                    <li>
                                            <span class="bold">Kies het materiaal</span>
                                            <span class="grey">{{$product_data->materials[0]->name}}</span>
                                   </li>
                                @endif
                                <!--li>
                                    <span class="bold">Kies het gewicht</span>
                                    <span class="grey">135 g/m²</span>
                                </li-->
                                <!--li>
                                    <span class="bold">Kies het aantal pagina's (incl. omslag)</span>
                                    <span class="grey">24 pagina's</span>
                                </li-->
                                <li>
                                    <span class="bold">Kies de cover / omslag</span>
                                    <span class="grey" id="cover">Selfcover</span>
                                </li>
                                <li>
                                    <span class="bold">Kies de oplage</span>
                                    <span class="grey">1 stuks</span>
                                </li>
                                <!--li>
                                    <span class="bold">Levertijd</span>
                                    <span class="grey">Normale levering</span>
                                </li>
                                <li>
                                    <span class="bold">Druktype</span>
                                    <span class="grey">Full color digitaal</span>
                                </li-->
                            </ul>
                            <ul class="buy-ul">
                                <li>
                                    <span class="left">Oplage per ontwerp</span>
                                    <span class="right">1</span>
                                </li>
                                <li>
                                    <span class="left">Prijs per ontwerp</span>
                                    <span class="right">&#8364; {{ round(($product_data->price*0.21),2) }}</span>
                                </li>
                                <li>
                                    <span class="left">Verzendkosten</span>
                                    <span class="right">Gratis</span>
                                </li>
                                <li>
                                    <span class="left">Totaal</span>
                                    <span class="right">&#8364; {{ round($product_data->price,2) }}</span>
                                </li>
                                <li>
                                    <span>&#8364; {{ round($product_data->price+round(($product_data->price*0.21),2),2) }} incl. btw</span>
                                </li>
                            </ul>
                            <button id="trigger_cart">Toevoegen aan winkelwagen</button>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

