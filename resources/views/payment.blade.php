@extends('main')

@section('content')

    <div class="container container-sm">
        <div class="col-lg-12 breadcrumbs">
            <a href="#">Home</a>
            <span href="">{{ $text }}</span>
        </div>
        <div class="col-lg-12 access-order">
            <h2>{{ $text }}</h2>
            <div class="__back-to-cart-link-wrap">
                <a href="/">Back to Games</a>
            </div>
        </div>
    </div>


@endsection