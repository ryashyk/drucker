@extends('main')



    @section('content')





        <div class="container container-sm">

            <div class="col-lg-12 breadcrumbs">

                <a href="/">Home</a>

                <span>Reviews</span>

            </div>

        </div>



        <div class="container container-sm">

            <div class="col-lg-12 reviews-wrap _with-paddings">

                <h2>Reviews</h2>

                <div class="row">

                    <div class="reviews">



                        @if(count($reviews)>0)

                            @foreach($reviews as $k)


                            <div class="review _border_white _width_half" itemscope="" itemtype="http://schema.org/Review">

                                <img class="avatar" src="../img/avatar1.jpg" width="54" height="54" alt="">

                                <div class="text-wrap">

                                    <div class="text-wrap--div">

                                        <div class="rating-wrap" itemprop="reviewRating">
                                            <input name="star{{ $k->id }}" type="radio" class="star" disabled="disabled"/>
                                            <input name="star{{ $k->id }}" type="radio" class="star" disabled="disabled"/>
                                            <input name="star{{ $k->id }}" type="radio" class="star" disabled="disabled">
                                            <input name="star{{ $k->id }}" type="radio" class="star" disabled="disabled"/>
                                            <input name="star{{ $k->id }}" type="radio" class="star" disabled="disabled" checked="checked"/>
                                        </div>

                                        <span class="review-username" itemprop="author">{{ $k->username }}</span>

                                    </div>

                                    <span class="__full-text" itemprop="reviewBody">{{ $k->content }}</span>

                                    <span class="title" itemprop="itemReviewed" itemprop="author">{{ $k->re_username }} : {{ $k->re_content }}</span>

                                    <span class="datetime" itemprop="datePublished">28.12.2016 12:36:48</span>

                                </div>

                            </div>



                            @endforeach



                        @endif







                    </div>

                </div>

                {{ $pagination }}

            </div>

        </div>



    @endsection