@extends('main')

@section('content')


    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="#">Contactgegevens</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="contactgegevens-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                    <div class="contact-left_container">
                        <h1>Contactgegevens</h1>
                        <ul class="bezoek-ul">
                            <li>
                                <h5>Bezoek/postadres</h5>
                            </li>
                            <li>
                                <span>MultiMedia Westland</span>
                            </li>
                            <li>
                                <span>Sand Ambachtstraat 136B</span>
                            </li>
                            <li>
                                <span>2691 BS  's-Gravenzande</span>
                            </li>
                        </ul>
                        <ul class="telefoon-ul">
                            <li>
                                <h5>Telefoon</h5>
                            </li>
                            <li>
                                <span>0174 -41 47 46</span>
                            </li>
                            <li>
                                <span>06-53 50 45 28</span>
                            </li>
                        </ul>
                        <ul class="bereikbaarheid-ul">
                            <li>
                                <h5>Bereikbaarheid</h5>
                            </li>
                            <li>
                                <span class="place">Maandag t/m donderdag</span>
                                <span class="data">8:30 tot 12.00 uur - 13:00 tot 17:00</span>
                            </li>
                            <li>
                                <span class="place">Vrijdag</span>
                                <span class="data">8:30 tot 12.00 uur</span>
                            </li>
                            <li>
                                <p>Na sluitingstijd altijd bereikbaar 24 uur per dag op 06-53504528</p>
                            </li>
                        </ul>
                        <ul class="email-ul">
                            <li>
                                <h5>Email adres</h5>
                            </li>
                            <li>
                                <a href="#">info@multimediawestland.nl</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-offset-1">
                    <div class="contact-right_container">
                        <form>
                            <h2>Neem contact met ons op</h2>
                            <section class="form-section">
                                <label>Aanhef</label>
                                <div class="checkbox-container">
                                    <input type="radio" name="#" id="radio" class="checkbox-input"/>
                                    <label for="radio" class="checkbox-label">Dhr.</label>
                                </div>
                                <div class="checkbox-container">
                                    <input type="radio" name="#" id="radio-1" class="checkbox-input"/>
                                    <label for="radio-1" class="checkbox-label">Mevr.</label>
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Naam</label>
                                <div class="input-container">
                                    <input type="text" name="" placeholder="Voornaam" >
                                    <input type="text" name="" placeholder="Achternaam" >
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Telefoonnummer</label>
                                <input type="text" name="" placeholder="Bijv. +31 11 22 33" >
                            </section>
                            <section class="form-section_error">
                                <span>wrong message</span>
                            </section>
                            <section class="form-section">
                                <label>E-mailadres</label>
                                <input type="text" name="">
                            </section>
                            <section class="form-section">
                                <label>Jouw vraag</label>
                                <textarea></textarea>
                            </section>
                            <section class="form-section_button">
                                <button type="submit">verzenden</button>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="map"  class="global-map_wrapper">
        </div>
        <div class="links-wrapper">
            <div class="container">
                <div class="links">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <h4>Huisstijl</h4>
                            <ul>
                                <li><a href="">Bedrukte-enveloppen</a></li>
                                <li><a href="">Briefpapier</a></li>
                                <li><a href="">Doordruksets</a></li>
                                <li><a href="">Notitie-en-schrijfblokken</a></li>
                                <li><a href="">Visitekaartjes-exclusief</a></li>
                                <li><a href="">With-complimentscards</a></li>
                                <li><a href="">Zelfklevende memoblokken</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <h4>Promotie</h4>
                            <ul>
                                <li><a href="">Aansichtkaarten/ Wenskaarten</a></li>
                                <li><a href="">Boekenleggers</a></li>
                                <li><a href="">Brochures / Magazines </a></li>
                                <li><a href="">Brochures /gelijmd</a></li>
                                <li><a href="">Flyers aanbiedingen</a></li>
                                <li><a href="">Flyers-ongevouwen</a></li>
                                <li><a href="">Folders-gevouwen</a></li>
                                <li><a href="">Aansichtkaarten/ Wenskaarten</a></li>
                                <li><a href="">Boekenleggers</a></li>
                                <li><a href="">Brochures / Magazines </a></li>
                                <li><a href="">Brochures /gelijmd</a></li>
                                <li><a href="">Flyers aanbiedingen</a></li>
                                <li><a href="">Flyers-ongevouwen</a></li>
                                <li><a href="">Folders-gevouwen</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <h4>Copy - printen</h4>
                            <ul>
                                <li><a href="">Canvasdoek</a></li>
                                <li><a href="">Prijzen copy-printen - inbinden</a></li>
                                <li><a href="">Printen full color</a></li>
                                <li><a href="">Printen zwart-wit</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <h4>Presentatie</h4>
                            <ul>
                                <li><a href="">Bierviltjes</a></li>
                                <li><a href="">Bouwtekeningen</a></li>
                                <li><a href="">L-banners</a></li>
                                <li><a href="">Placemats</a></li>
                                <li><a href="">Posters v.a 100 stuks</a></li>
                                <li><a href="">presentatiemappen</a></li>
                                <li><a href="">Rollup banners</a></li>
                                <li><a href="">X-banners</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <h4>Buitenreclame</h4>
                            <ul>
                                <li><a href="">Reclameborden</a></li>
                                <li><a href="">Spandoeken</a></li>
                                <li><a href="">Stoepborden</a></li>
                            </ul>
                            <h4>Stickers</h4>
                            <ul>
                                <li><a href="">Stickers</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <h4>Relatiegeschenken</h4>
                            <ul>
                                <li><a href="">Accessoires</a></li>
                                <li><a href="">Beeld & geluid</a></li>
                                <li><a href="">Brief - en schrijfpapier</a></li>
                                <li><a href="">Broeken</a></li>
                                <li><a href="">Drinkwaren</a></li>
                                <li><a href="">Gezondheid en verzorging</a></li>
                                <li><a href="">Horloges</a></li>
                                <li><a href="">Kantoor & business</a></li>
                                <li><a href="">Accessoires</a></li>
                                <li><a href="">Beeld & geluid</a></li>
                                <li><a href="">Brief - en schrijfpapier</a></li>
                                <li><a href="">Broeken</a></li>
                                <li><a href="">Drinkwaren</a></li>
                                <li><a href="">Gezondheid en verzorging</a></li>
                                <li><a href="">Horloges</a></li>
                                <li><a href="">Kantoor & business</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection