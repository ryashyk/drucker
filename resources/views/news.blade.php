@extends('main')

@section('content')

    <div class="container container-sm">
        <div class="col-lg-12 breadcrumbs">
            <a href="/">Home</a>
            <span>News</span>
        </div>
    </div>

    <div class="container container-sm">
        <div class="news-wrap _with-paddings col-lg-12">
            <h2>News</h2>
            <div class="news">

                    @foreach($news as $k)
                        <a href="/news/news_is-{{$k->id}}" class="news-item-a _border_white _size_b">
                            <span class="title">{{  $k->info_title  }}</span>
                            <span class="date">{{ date('Y-m-d', strtotime($k->option_date)) }}</span>
                            <span class="__news-text">{!! Str::words(strip_tags($k->info_content),38) !!}</span>
                        </a>
                    @endforeach

            </div>

            {{ $pagination }}
        </div>
    </div>
    </div>


@endsection