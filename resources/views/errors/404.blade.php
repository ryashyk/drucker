<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Page / Record not found.</title>

    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/reset.css')}}">

    <link rel="stylesheet" href="{{asset('/css/style.css')}}">

    <link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
</head>
<body>
<main class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="error-404_wrapper">
                    <div class="logo">
                        <a href="/">
                            <img src="/img/logo.jpg" alt="">
                        </a>
                    </div>
                </div>
                <div class="error-404_container">
                    <p>We hebben alles voor u nagekeken maar de opgevraagde pagina is helaas niet beschikbaar</p>
                    <a href="/">Terug naar de homepage</a>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery-3.1.1.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcoPU5bL4ZG-kNgZkAA7IqCynhq5nLCbs&signed_in=true&libraries=places&language=en"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>

<!-- Arcticmodal -->
<script src="/js/jquery.arcticmodal-0.3.min.js"></script>

<!-- Slick-->
<script src="/js/slick.min.js"></script>

<!-- Select -->
<script type="text/javascript" src="/js/select2.min.js"></script>

<!-- Anonym.js-->
<script src="/js/anonym.js"></script>
</body>