@extends('main')

@section('content')


    @if(count($slider)>0)
        <div class="big_slider-wrp">
        @foreach($slider as $k)

                <div>
                    <img src="/storage/uploads{{$k->photo_path}}" alt="{{$k->name}}">
                    <div class="container">
                        <div class="slider_info">
                            {!! $k->description !!}
                        </div>
                    </div>
                </div>


        @endforeach
        </div>
    @endif
    <div class="product-wrapper">
        <div class="container">
            <div class="product">
                <h3>Aanbevolen Product</h3>
                <div class="aanbevolen_product-carousel">
                    @if(count($recommended)>0)
                        @foreach($recommended as $k)
                            <div>
                                <figure>
                                    <a href="/product-one/id-{{ $k->id }}"><img src="/public/uploads/product/{{$k->photo_name}}" alt=""></a>
                                </figure>
                                <div>
                                    <h4><a href="/product-one/id-{{ $k->id }}">{{ $k->name }}</a></h4>
                                    <p>{!! Str::words(strip_tags($k->info),8) !!}</p>
                                    <strong>€ {{ $k->price }}</strong>
                                    <a href="/product-one/id-{{ $k->id }}">Bekijk nu</a>
                                </div>

                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="banners">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <a href="">
                                <img src="/img/1_banner.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <a href="">
                                <img src="/img/2_banner.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <h3>Populairste producten</h3>
                <div class="populairste_producten-carousel">
                            @if(count($popular)>0)
                                @foreach($popular as $k)
                                    <div>
                                        <figure>
                                            <a href="/product-one/id-{{ $k->id }}"><img src="/public/uploads/product/{{$k->photo_name}}" alt=""></a>
                                        </figure>
                                        <div>
                                            <h4><a href="/product-one/id-{{ $k->id }}">{{ $k->name }}</a></h4>
                                            <p>{!! Str::words(strip_tags($k->info),8) !!}</p>
                                            <strong>€ {{ $k->price }}</strong>
                                            <a href="/product-one/id-{{ $k->id }}">Bekijk nu</a>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                </div>
            </div>
        </div>
    </div>


    <div class="voordel-wrapper">
        @if(count($why_choose)>0)
        <div class="container">
            <div class="voordel">
                <h3>Uw voordeel</h3>
                <div class="row">
                    @foreach($why_choose as $k)
                        <div>
                            <figure>
                                <img src="/storage/uploads{{$k->photo_hash}}" alt="{{ $k->name }}">
                            </figure>
                            <span>{{ $k->name }}</span>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
        @endif
    <div class="comments-wrapper">
        <div class="container">
            <div class="comments">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <h4>Daarom kies je MultiMedia Westland</h4>
                        <p>MultiMedia Westland is een toonaangevende drukwerk leverancier met meer dan 35 jaar ervaring. Wij onderscheiden ons door, flexibiliteit, snelheid, persoonlijke begeleiding en niet onbelangrijk de prijs. U kunt rekenen op een complete service met print en kopieer faciliteiten en het leveren van perfect drukwerk. Levering door geheel Nederland doen wij GRATIS. Heeft u problemen om de layout of werktekening op juiste manier aan te leveren? Pak de telefoon en waag en belletje dan zoeken we samen naar een oplossing.</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="comments-carousel">
                            <div class="comment_slide">
                                <a href="" style="float: left; width:100%;">
                                    <div class="comment_wrapper">
                                        <h5>Perfecte service!</h5>
                                        <div class="stars">
                                            <img src="/img/stars.png" alt="">
                                        </div>
                                        <p>Even niet op letten en je drukwerk wordt een flop. Met dank aan het design teamvan MultiMedia Westland die zeer attent reageerde op een afbeelding in de folder, die een te lage resolutie had waardoor de afbeelding onduidelijk en wazig zou worden. MultiMedia Westland deed mij een geweldig voorstel. Zeer attent gereageerd en snel afgewikkeld. Kijk en dat noem ik nou service!!</p>
                                        <div class="human_info">
                                            <img src="/img/user-icon.png" alt="">
                                            <span>Marcel Stephan</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <!--div class="comment_slide">
                                <div class="comment_wrapper">
                                    <h5>Perfecte service!</h5>
                                    <div class="stars">
                                        <img src="/img/stars.png" alt="">
                                    </div>
                                    <p>Superaardige aktie van MultiMedia Westland! Door stommiteit bestanden verkeerd aangeleverd. MultiMedia Westland had alle begrip en deed mij een geweldig voorstel. Zeer sympathiek en snel afgewikkeld. Kan niet beter.</p>
                                    <div class="human_info">
                                        <img src="/img/photo.jpg" alt="">
                                        <span>Marcel Stephan</span>
                                    </div>
                                </div>
                            </div-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

