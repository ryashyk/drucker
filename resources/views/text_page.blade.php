@extends('main')

@section('content')
<div class="container container-sm">
    <div class="col-lg-12 breadcrumbs">
        <a href="/">Home</a>
        <a href="/news">News</a>
        <span>{!! $page->info_title !!}</span>
    </div>
</div>

<div class="container container-sm">
    <div class="col-lg-12 text-page">
        <div class="__text-wrap">
            <h1>{!! $page->info_title !!}</h1>

            {!! $page->info_content !!}

            <hr>

        </div>

    </div>
</div>
@endsection