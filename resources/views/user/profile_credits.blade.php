@extends('user.index')



@section('content_us')

    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 profile-main">
        <h1>Available credits</h1>
        <div class="profile-slim-block">
                <span class="profile-hint">
                    Available credits - Here is your available credits you can use to make purchases or reduce the cost of your order.
                </span>
            <div class="your-credits">
                <span class="__title">Your credits</span>
                <span class="__sum">{{ $user_data->total_credits }}</span>
            </div>
            <div class="how-to-use">
                <h2>How to use credits?</h2>

                <p>How to use credits? - Each time you make a purchase 10% of it's value returns to you as a credits. You can use the credits when you making purchase in our website, you can by only for credits or you can add them to your order and reduce the cost for it's amounts.</p>
                <p>1 credit is 1$ So when you have 10 credits you can buy goods for 10$ or reduce the 50$ cost to 40$ for example using your 10 credits.</p>
                <p>There is a special section for the discount when you checkout, where you can chose one of many discount options we provide.</p>
            </div>
        </div>
    </div>
@endsection


