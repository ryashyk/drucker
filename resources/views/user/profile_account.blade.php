@extends('user.index')



@section('content_us')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Mijn gegeven</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="account-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                    <div class="contact-left_container">
                        <h1>Mijn gegeven</h1>
                        <form>
                            <div class="gegevens-container">
                                <h3>Gegevens</h3>
                                <div class="input-label_container">
                                    <label class="main">Aanhef</label>
                                    <div class="input-container">
                                        <label>Dhr.
                                            <input type="radio"  {{ ($user_data->sex=='male')? 'checked="checked"' :'' }} name="sex">
                                            <span class="grey-circle"></span>
                                        </label>
                                        <label>Mevr.
                                            <input type="radio"  {{ ($user_data->sex=='female')? 'checked="checked"' :'' }} name="sex">
                                            <span class="grey-circle"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>Voornaam</label>
                                    <div class="input-container">
                                        <input type="text" value="{{ $user_data->fast_name }}" name="fast_name">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>Achternaam</label>
                                    <div class="input-container">
                                        <input type="text" value="{{ $user_data->last_name }}" name="last_name">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>E-mailadres</label>
                                    <div class="input-container">
                                        <input type="text" value="{{ $user_data->mail }}" readonly="readonly">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>Telefoon</label>
                                    <div class="input-container">
                                        <input type="text" value="{{ $user_data->phone }}" name="phone">
                                    </div>
                                </div>

                                <div class="input-label_container">
                                    <label class="not-main">Geboortedatum</label>
                                    <div class="input-container">
                                        <input type="text" id="datepicker">
                                    </div>
                                </div>
                                <div class="button-wrapper">
                                    <button type="submit" class="buy">opslaan</button>
                                </div>
                            </div>

                            </form>
                            <form>
                            <div class="opnieuw-container">
                                <h3>Wachtwoord opnieuw instellen</h3>
                                <div class="input-label_container">
                                    <label class="not-main">Wachtwoord</label>
                                    <div class="input-container">
                                        <input type="password">
                                    </div>
                                </div>
                                <div class="button-wrapper">
                                    <button type="submit" class="buy">opslaan</button>
                                </div>
                            </div>
                            </form>
                        <form>
                            <div class="accountgegevens-container">
                                <h3>Accountgegevens</h3>

                                <div class="input-label_container">
                                    <label>Type account</label>
                                    <div class="input-container">
                                        <input type="text" name="">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>Bedrijf</label>
                                    <div class="input-container">
                                        <input type="text" value="{{ $user_data->company }}"  name="company">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label class="not-main">Kvk-nummer</label>
                                    <div class="input-container">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label class="not-main">BTW-nummer</label>
                                    <div class="input-container">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>Land</label>
                                    <div class="input-container">
                                        <input type="text" name="country" value="{{ $user_data->country }}">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>Adres</label>
                                    <div class="input-container">
                                        <input type="text" name="addsess" value="{{ $user_data->addsess }}">
                                    </div>
                                </div>
                                <div class="input-label_container">
                                    <label>Postcode</label>
                                    <div class="input-container">
                                        <input type="text" value="{{ $user_data->postcode }}" name="postcode">
                                    </div>
                                </div>
                            </div>
                            <div class="button-wrapper">
                                <button type="submit" class="buy">opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-lg-offset-2">
                    <div class="right-container">
                        @include('block.top_user_info')
                        @include('block.r_profile_menu')
                        @include('block.r_contact')
                    </div>
                </div>
            </div>
        </div>
@endsection