@extends('user.index')



@section('content_us')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="/profile_user">Mijn account</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="/profile_orders">Mijn orders</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Order details</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="profile-order_wrapper">
        <div class="container profile-order_container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                    <div class="left-container pdf-content" style="background: #fff;">
                        <section class="header-left_container">
                            <h1>Order 100003761</h1>
                            <ul id="ulHide">
                                <li>
                                    <i class="fa fa-folder-o" aria-hidden="true"></i>
                                    <a href="#" id="cmd">Download orderbevestiging</a>
                                </li>
                                <li>
                                    <a href="#">Order annuleren</a>
                                </li>
                            </ul>
                        </section>
                        <div class="upload-wrapper">
                            <form>
                                <ul>
                                    <li>
                                        <label>Besteldatum</label>
                                        <p>{{ $order->order_id }} (door {{ $order->user_info }})</p>
                                    </li>
                                    <li>
                                        <label>PO / Opdrachtnummer</label>
                                        <p>Geen opmerkingen</p>
                                    </li>
                                    <li>
                                        <label>Referentie</label>
                                        <p>Geen opmerkingen</p>
                                    </li>
                                    <li>
                                        <label>Opmerking</label>
                                        <p>Geen opmerkingen</p>
                                    </li>
                                </ul>
                                <a href="/profile_order_upload/{{ $order->order_id }}" class="upload-link">BESTANDEN UPLOADEN</a>
                            </form>
                        </div>
                        <div class="producten-wrapper">
                            <form>
                                <h4>Producten</h4>
                                <section>
                                    <a href="#">{{ $order->product_name }}</a>
                                    <span>&#8364 {{ $order->total_price }}</span>
                                </section>
                                <button class="werkdagen-link" type="submit">6 werkdagen</button>
                            </form>
                        </div>
                        <div class="betalen-wrapper">
                            <h4>Betalen</h4>
                            <ul>
                                <li>
                                    <label>Subtotaal</label>
                                    <span>&#8364 {{ $order->total_price }}</span>
                                </li>
                                <li>
                                    <label>Betalingswijze ({{ $order->pay_type }})</label>
                                    <span>&#8364 0,00</span>
                                </li>
                                <li>
                                    <label>Verzendkosten</label>
                                    <span>GRATIS</span>
                                </li>
                                <li>
                                    <label>Klantenkorting</label>
                                    <span>&#8364 0,00</span>
                                </li>
                                <li>
                                    <label>Betaald</label>
                                    <span>&#8364 0,00</span>
                                </li>
                                <li>
                                    <label>Totaal excl. BTW</label>
                                    <span>&#8364 {{ $order->total_price }}</span>
                                </li>
                                <li>
                                    <label>BTW 21%</label>
                                    <span>&#8364 {{ round(($order->total_price*0.21),2) }}</span>
                                </li>
                                <li>
                                    <label>Totaal</label>
                                    <span>&#8364 {{ $order->total_price+(round(($order->total_price*0.21),2)) }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="editor"></div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <div class="right-container">
                        @include('block.top_user_info')
                        @include('block.r_profile_menu')
                        @include('block.r_contact')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


