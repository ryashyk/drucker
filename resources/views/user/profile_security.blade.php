@extends('user.index')



@section('content_us')

    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 profile-main">
        <h1>Account security</h1>
        <div class="profile-slim-block">
                <span class="profile-hint">
                    Here you can change your password and email address
                </span>
            <div class="password-form-wrap">
                <form>
                    <span class="__title">Change password</span>
                    <input type="password" placeholder="Current password">
                    <input type="password" placeholder="New password">
                    <input type="password" placeholder="Confirm password">
                    <button type="submit">Save</button>
                </form>
            </div>
            <div class="email-form-wrap">
                <form>
                    <span class="__title">Change email address</span>
                    <input type="email" placeholder="New email address">
                    <input type="password" placeholder="Password">
                    <button type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
