@extends('main')



@section('content')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Mijn orders</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="profile-mijn_wrapper">
        <div class="container profile-mijn_container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                    <div class="left-container">
                        <h1>Orders</h1>
                        <div class="order-tabs_wrapper">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Alle</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">In behandeling</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Vooruitbetaling nodig</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Bestanden aanleveren</a></li>
                                <li role="presentation"><a href="#settings2" aria-controls="settings2" role="tab" data-toggle="tab">Verzonden</a></li>
                                <li role="presentation"><a href="#settings3" aria-controls="settings3" role="tab" data-toggle="tab">Geannuleerd</a></li>
                            </ul>
                            <div class="search-select_order">
                                <div class="searcher">
                                    <form method="GET" action="/profile_user">
                                    <input type="search" name="id">
                                        {{ csrf_field() }}
                                    <button type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                    </form>

                                </div>
                                <div class="select">
                                    <select class="js-example-basic-single">
                                        <option value="AL">Op datum (niuwste eerst)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    @if(count($orders)>0)

                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th class="bestelnr">Bestelling</th>
                                                <th class="datum">Besteld</th>
                                                <th class="odertotaal">Totaalprijes</th>
                                                <th class="status">Status</th>
                                                <th class="ship">Ontvanger</th>
                                            </tr>
                                            @foreach($orders as $k)
                                                <tr>
                                                    <td class="bestelnr"><a href="/profile_order/{{ $k->order_id }}">{{ $k->order_id }}</a></td>
                                                    <td class="datum">{{ date($k->date_order) }}</td>
                                                    <td class="odertotaal"> &#8364 {{ $k->total_price }}</td>
                                                    <td class="status {{ $k->status }}"><span>{{ $k->status }}</span></td>
                                                    <td class="ship"><a href="/profile_order/{{ $k->order_id }}">Toon besteling</a></td>
                                                </tr>
                                            @endforeach

                                        </table>
                                    </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    @if(count($orders)>0)

                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="bestelnr">Bestelling</th>
                                                    <th class="datum">Besteld</th>
                                                    <th class="odertotaal">Totaalprijes</th>
                                                    <th class="status">Status</th>
                                                    <th class="ship">Ontvanger</th>
                                                </tr>
                                                @foreach($orders as $k)
                                                    @if($k->status=='In behandeling')
                                                        <tr>
                                                            <td class="bestelnr"><a href="/profile_order/{{ $k->order_id }}">{{ $k->order_id }}</a></td>
                                                            <td class="datum">{{ date($k->date_order) }}</td>
                                                            <td class="odertotaal"> &#8364 {{ $k->total_price }}</td>
                                                            <td class="status {{ $k->status }}"><span>{{ $k->status }}</span></td>
                                                            <td class="ship"><a href="/profile_order/{{ $k->order_id }}">Toon besteling</a></td>
                                                        </tr>
                                                    @endif
                                                @endforeach

                                            </table>
                                        </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                    @if(count($orders)>0)

                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="bestelnr">Bestelling</th>
                                                    <th class="datum">Besteld</th>
                                                    <th class="odertotaal">Totaalprijes</th>
                                                    <th class="status">Status</th>
                                                    <th class="ship">Ontvanger</th>
                                                </tr>
                                                @foreach($orders as $k)
                                                    @if($k->status=='Vooruitbetaling nodig')
                                                        <tr>
                                                            <td class="bestelnr"><a href="/profile_order/{{ $k->order_id }}">{{ $k->order_id }}</a></td>
                                                            <td class="datum">{{ date($k->date_order) }}</td>
                                                            <td class="odertotaal"> &#8364 {{ $k->total_price }}</td>
                                                            <td class="status {{ $k->status }}"><span>{{ $k->status }}</span></td>
                                                            <td class="ship"><a href="/profile_order/{{ $k->order_id }}">Toon besteling</a></td>
                                                        </tr>
                                                    @endif
                                                @endforeach

                                            </table>
                                        </div>
                                    @endif


                                </div>
                                <div role="tabpanel" class="tab-pane" id="settings">
                                    @if(count($orders)>0)

                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="bestelnr">Bestelling</th>
                                                    <th class="datum">Besteld</th>
                                                    <th class="odertotaal">Totaalprijes</th>
                                                    <th class="status">Status</th>
                                                    <th class="ship">Ontvanger</th>
                                                </tr>
                                                @foreach($orders as $k)
                                                    @if($k->status=='Bestanden aanleveren')
                                                        <tr>
                                                            <td class="bestelnr"><a href="/profile_order/{{ $k->order_id }}">{{ $k->order_id }}</a></td>
                                                            <td class="datum">{{ date($k->date_order) }}</td>
                                                            <td class="odertotaal"> &#8364 {{ $k->total_price }}</td>
                                                            <td class="status {{ $k->status }}"><span>{{ $k->status }}</span></td>
                                                            <td class="ship"><a href="/profile_order/{{ $k->order_id }}">Toon besteling</a></td>
                                                        </tr>
                                                    @endif
                                                @endforeach

                                            </table>
                                        </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="settings2">
                                    @if(count($orders)>0)

                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="bestelnr">Bestelling</th>
                                                    <th class="datum">Besteld</th>
                                                    <th class="odertotaal">Totaalprijes</th>
                                                    <th class="status">Status</th>
                                                    <th class="ship">Ontvanger</th>
                                                </tr>
                                                @foreach($orders as $k)
                                                    @if($k->status=='Verzonden')
                                                        <tr>
                                                            <td class="bestelnr"><a href="/profile_order/{{ $k->order_id }}">{{ $k->order_id }}</a></td>
                                                            <td class="datum">{{ date($k->date_order) }}</td>
                                                            <td class="odertotaal"> &#8364 {{ $k->total_price }}</td>
                                                            <td class="status {{ $k->status }}"><span>{{ $k->status }}</span></td>
                                                            <td class="ship"><a href="/profile_order/{{ $k->order_id }}">Toon besteling</a></td>
                                                        </tr>
                                                    @endif
                                                @endforeach

                                            </table>
                                        </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="settings3">
                                    @if(count($orders)>0)

                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th class="bestelnr">Bestelling</th>
                                                    <th class="datum">Besteld</th>
                                                    <th class="odertotaal">Totaalprijes</th>
                                                    <th class="status">Status</th>
                                                    <th class="ship">Ontvanger</th>
                                                </tr>
                                                @foreach($orders as $k)
                                                    @if($k->status=='Geannuleerd')
                                                        <tr>
                                                            <td class="bestelnr"><a href="/profile_order/{{ $k->order_id }}">{{ $k->order_id }}</a></td>
                                                            <td class="datum">{{ date($k->date_order) }}</td>
                                                            <td class="odertotaal"> &#8364 {{ $k->total_price }}</td>
                                                            <td class="status {{ $k->status }}"><span>{{ $k->status }}</span></td>
                                                            <td class="ship"><a href="/profile_order/{{ $k->order_id }}">Toon besteling</a></td>
                                                        </tr>
                                                    @endif
                                                @endforeach

                                            </table>
                                        </div>
                                    @endif
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <div class="right-container">
                        @include('block.top_user_info')
                        @include('block.r_profile_menu')
                        @include('block.r_contact')

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection