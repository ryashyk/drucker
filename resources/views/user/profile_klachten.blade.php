@extends('user.index')



@section('content_us')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Mijn klachten</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="klachten-wrapper">
        <div class="container profile-mijn_container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                    <div class="left-container">
                        <h1>Mijn klachten</h1>
                        <textarea placeholder="Je hebt nog klacht ingediend"></textarea>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <div class="right-container">
                        @include('block.top_user_info')
                        @include('block.r_profile_menu')
                        @include('block.r_contact')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


