@extends('user.index')



@section('content_us')
<div class="ul-wrapper">
    <div class="container">
        <div class="row">
            <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ul>
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </li>
                    <li class="active">
                        <a href="#">Upload bestand</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="guillotine-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                <div class="left-container">
                    <h1>Upload bestand(en)</h1>
                    <div class="upload-plugin">
                        <div id='content' class="content">
                            <div class='frame'>
                            </div>

                            <div id='controls' class="upload-control_panel">
                                <button id='rotate_left' class="rotate_left" type='button' title='Rotate left'><i class='fa fa-rotate-left'></i></button>
                                <button id='zoom_out'  class="zoom_out"   type='button' title='Zoom out'><i class='fa fa-search-minus'></i></button>
                                <button id='fit'   class="fit"       type='button' title='Fit image'><i class='fa fa-arrows-alt'></i></button>
                                <button id='zoom_in' class="zoom_in"     type='button' title='Zoom in'><i class='fa fa-search-plus'></i></button>
                                <button id='rotate_right' class="rotate_right" type='button' title='Rotate right'><i class='fa fa-rotate-right'></i></button>
                                <form id="upload_product_photo">
                                    <label class="file-maker">
                                        <i class="fa fa-folder-o" aria-hidden="true"></i>
                                        <input name="photo" accept="image/*" type="file" multiple="true">
                                        <input name="action" value="add_product_photo" type="hidden">
                                        {{ csrf_field() }}
                                    </label>
                                </form>
                            </div>

                            <ul id='data'>
                                <div class='column'>
                                    <li>x: <span id='x'></span></li>
                                    <li>y: <span id='y'></span></li>
                                </div>
                                <div class='column'>
                                    <li>width:  <span id='w'></span></li>
                                    <li>height: <span id='h'></span></li>
                                </div>
                                <div class='column'>
                                    <li>scale: <span id='scale'></span></li>
                                    <li>angle: <span id='angle'></span></li>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                <div class="right-container">
                    <div class="uploaden-container">
                        <span>Order {{ $order->order_id }}</span>
                        <button type="submit">uploaden</button>
                    </div>
                    <!--div class="visitkaartje-wrapper">
                        <span class="dark">Visitkaartje.pdf<i class="fa fa-times" aria-hidden="true" id="close"></i></span>
                        <span class="grey">Klaar om geplaatst te worden</span>
                        <div class="upload-img">
                            <img src="/img/upload-1.png" alt="">
                            <article>
                                <p>109 x 80.5 mm</p>
                                <span>1</span>
                            </article>
                        </div>
                    </div-->
                    <div class="controleren-wrapper">
                        <h5>Controleren en afronden</h5>
                        <span>Controleer de aandachtspunten</span>
                        <!--label>
                            <input type="checkbox">
                            Bestand is automatisch omgezet naar CMYK
                        </label>
                        <span>Eindkwaliteit controleren</span>
                        <a href="#" class="pdf">Bekijk pdf voorbeeld</a-->
                        <label>
                            <input type="checkbox">
                            Ik ben akkord met omzetting(en) en positionering
                        </label>
                        <span>Afronden van het aanleveren</span>
                        <a href="#" class="complete">Aanleveren afronden</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 @endsection