@extends('user.index')



@section('content_us')

    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="/profile_user">Mijn account</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Mijn fakturen</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="invoices-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                    <div class="contact-left_container">
                        <h1>Mijn fakturen</h1>
                        <p>In dit overzicht vind je al je facturen</p>
                        <div class="table-responsive">
                            @if(count($orders)>0)
                            <table class="table">
                                @foreach($orders as $k)
                                    <tr>
                                        <td class="ship"><a href="/profile_order/{{$k->order_id}}">Download factuur</a></td>
                                        <td class="bestelnr"><a href="#">{{$k->order_id}}</a></td>
                                        <td class="datum">{{ $k->date_order }}</td>
                                        <td class="odertotaal"> &#8364 {{$k->total_price}}</td>
                                        <td class="status"><span>Verzonden</span></td>
                                    </tr>
                                @endforeach
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                    <div class="right-container">
                        @include('block.top_user_info')
                        @include('block.r_profile_menu')
                        @include('block.r_contact')
                    </div>
                </div>
            </div>
        </div>



@endsection