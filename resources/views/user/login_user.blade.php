@extends('main')

@section('content')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="inloggen_content col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4>Inloggen</h4>
            <form class="form_open_login" autocomplete="off">
                <span>Ik heb een account</span>
                <input type="hidden" name="action" value="login_user">
                {{ csrf_field() }}
                <label class="icon-envelope"><input type="email" name="email" placeholder="E-mailadres"></label>
                <label class="icon-lock-open"><input type="password" name="password" placeholder="Wachtwoord"></label>
                <a class="register" href="/registration_user">Registreren</a>
                <a class="forgott_pass" href="/html/wachtwoord-vergeten.html">Wachtwoord vergeten?</a>
                <button type="submit">Inloggen</button>
            </form>
        </div>
    </div>
    </div>
@endsection

