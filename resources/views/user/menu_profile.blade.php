@extends('user.index')

@section('menu_profile')
    <div class="col-lg-3 col-md-4 profile-nav">
        <nav>
            <a href="/profile_user" class="_active">Account information</a>
            <a href="#">Orders</a>
            <a href="#">Inbox</a>
            <a href="/profile_coupons">Coupons</a>
            <a href="#">Account level</a>
            <a href="#">Available credits</a>
            <a href="#">Account security</a>
        </nav>
    </div>
@endsection