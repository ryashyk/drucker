@extends('main')

@section('content')
    <div class="ul-wrapper">
        <div class="container">
            <div class="row">
                <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="active">
                            <a href="#">Aanmelden als nieuwe klant</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="reg-wrapper">
        <div class="container left-right_container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="contact-left_container">
                        <h1>Aanmelden als nieuwe klant</h1>
                        <form id="registration_form" data-action="/ajax">
                            <section class="form-section">
                                <label>Aanhef</label>
                                <div class="checkbox-container">
                                    <input type="radio"  name="sex" value="male" id="radio" checked="checked" class="checkbox-input"/>
                                    <label for="radio" class="checkbox-label">Dhr.</label>
                                </div>
                                <div class="checkbox-container">
                                    <input type="radio" name="sex" value="female" id="radio-1" class="checkbox-input"/>
                                    <label for="radio-1" class="checkbox-label">Mevr.</label>
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Naam</label>
                                <div class="input-container">
                                    <div>
                                        <input type="text" name="fast_name" placeholder="Voornaam" >
                                    </div>
                                    <div>
                                        <input type="text" name="last_name" placeholder="Achternaam" >

                                    </div>
                                    <input name="action" value="registration_user" type="hidden">
                                    {{ csrf_field() }}

                                </div>
                            </section>
                            <section class="form-section">
                                <label>Land</label>
                                <div>
                                    <select name="country" class="select js-example-basic-single">
                                        <option value="AL">Nederland</option>
                                        <option value="WY">Nederland</option>
                                    </select>
                                </div>

                            </section>
                            <section class="form-section">
                                <label class="without-star">Bedrijf</label>
                                <div>
                                    <input type="text" name="company" placeholder="Voornaam" >

                                </div>
                            </section>
                            <section class="form-section">
                                <label>Postcode</label>
                                <div>
                                    <input type="text" name="postcode" placeholder="Postcode" >
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Huisnummer</label>
                                    <div>
                                        <input type="text" name="addsess" placeholder="Nummer" class="short">
                                    </div>
                            </section>
                            <section class="form-section">
                                <label>Telefoonnummer</label>
                                <div>
                                    <input type="text" name="phone" placeholder="Bijv. +31 11 22 33" >

                                </div>
                            </section>
                            <section class="form-section">
                                <label>E-mailadres</label>
                                <div>
                                    <input type="text" name="mail" type="email">
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Wachtwoord</label>
                                <div>
                                    <input type="password" name="password">
                                </div>
                            </section>
                            <section class="form-section">
                                <label>Wachtwoord controle</label>
                                <div>
                                    <input type="password" name="password_confirmation">
                                </div>
                            </section>
                            <section class="form-section_radio">
                                <div class="checkbox-container">
                                    <input type="radio" name="#" id="radio-2" class="checkbox-input"/>
                                    <label for="radio-2" class="checkbox-label">Ik heb Privacy Statement gelezen en accepteer deze.</label>
                                </div>
                                <div class="checkbox-container">
                                    <input type="radio" name="#" id="radio-3" class="checkbox-input"/>
                                    <label for="radio-3" class="checkbox-label">Ik wil per e-mail over nieuwtjes en aanbiedingen op de hoogte gehounden worden. Opzeggen is altijd mogelijk.</label>
                                </div>
                            </section>
                            <section class="form-section_button">
                                <button type="submit">Registreer</button>
                            </section>
                        </form>
                    </div>
                </div>

            </div>
        </div>

@endsection

