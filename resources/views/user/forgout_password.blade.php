@extends('main')

@section('content')
<div class="ul-wrapper">
    <div class="container">
        <div class="row">
            <div class="ul-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ul>
                    <li>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </li>
                    <li class="active">
                        <a href="#">Вen mijn wachtwoord vergeten</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="nieuw-wachtwoord_wrapper">
    <div class="container nieuw-wachtwoord_container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="left-container">
                    <h1>Вen mijn wachtwoord vergeten</h1>
                    <div class="password-wrapper">
                        <div class="password-container">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <label>Email</label>
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-4 col-lg-4">
                                <input type="text" name="">
                                <button type="submit">sturen</button>
                                <section class="form-section_error">
                                    <span>wrong message</span>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection