<aside class="welcome-container">
    <ul>
        <li>Welkom {{ $user->fast_name }}!</li>
        <li><a href="/logout_user">Uitloggen</a></li>
        <li><a href="/profile_user">Bekijk al je orders</a></li>
        <li><div class="li-content"><span class="icon-sec"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="dig">{{ Cart::count() }}</span></span><a href="/cart">producten in winkelwagen</a></div></li>
        <li><div class="li-content"><span class="icon-sec"><i class="fa fa-clock-o" aria-hidden="true"></i><span class="dig">0</span></span><a href="/profile_user#settings2">orders in verwerking</a></div></li>
        <li><div class="li-content"><span class="icon-sec"><i class="fa fa-cloud-upload" aria-hidden="true"></i><span class="dig">0</span></span><a href="/profile_user#settings">bestanden aan te leveren</a></div></li>
        <li><div class="li-content"><span class="icon-sec"><i class="fa fa-credit-card-alt" aria-hidden="true"></i><span class="dig">0</span></span><a href="/profile_user#messages">vooruitbetalingen nodig</a></div></li>
        <li><div class="li-content"><span class="icon-sec"><i class="fa fa-file-o" aria-hidden="true"></i><span class="dig">0</span></span><a href="/profile_user#profile">in behandeling</a></div></li>
    </ul>
</aside>