<aside class="contact-container">
    <section>
        <h4 class="personal-h4">Persoonlijk advies</h4>
        <ul>
            <li><i class="fa fa-phone" aria-hidden="true"></i>0174 - 414746</li>
            <li><a href="/contact">Stuur een mail</a></li>
        </ul>
    </section>
    <section>
        <ul>
            <li><i class="fa fa-clock-o" aria-hidden="true"></i>Bereikbaar tot 17:30 uur</li>
            <li><a href="/page/openingstijden">Bekijk openingstijden</a></li>
        </ul>
    </section>
    <section>
        <ul>
            <li><i class="fa fa-comments-o" aria-hidden="true"></i>Chat</li>
            <li><a href="/chat">Chat beschikbaar</a></li>
        </ul>
    </section>
    <section>
        <ul>
            <li><i class="fa fa-life-ring" aria-hidden="true"></i>Vraag & antwoord</li>
            <li><a href="/faq">Meestgestelde vragen</a></li>
            <li><a href="/page/aanleverspecificaties">Aanleverspecificaties</a></li>
        </ul>
    </section>
</aside>