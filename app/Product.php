<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Image;
use Cache;
use Carbon;
class Product extends Model
{


    /**
     * @return product by id
     */
     public function getProduct($id){

         $product =  DB::table('products as p')
                     ->where('p.id', $id)
                     ->leftJoin('uploads', 'uploads.id', '=', 'p.cover')
                     ->select('p.*','uploads.hash','uploads.name as photo_name','uploads.path')
                     ->orderBy('p.id', 'desc')
                     ->first();
         $product->formats = array();
         foreach(json_decode($product->format) as $k=>$v){

             $product->formats[$k] = DB::table('fromats as f')
                                         ->where('f.id', $v)
                                         ->leftJoin('uploads', 'uploads.id', '=', 'f.cover')
                                         ->select('f.*','uploads.hash','uploads.name as photo_name','uploads.path')
                                         ->first();

         }

         $product->printings = array();
         foreach(json_decode($product->printing) as $k=>$v){

             $product->printings[$k] = DB::table('the_printings as t')
                 ->where('t.id', $v)
                 ->leftJoin('uploads', 'uploads.id', '=', 't.name')
                 ->select('t.*','uploads.hash','uploads.name as photo_name','uploads.path')
                 ->first();

         }
         $product->materials = array();
         foreach(json_decode($product->material) as $k=>$v){

             $product->materials[$k] = DB::table('materials as m')
                 ->where('m.id', $v)
                 ->leftJoin('uploads', 'uploads.id', '=', 'm.cover')
                 ->select('m.*','uploads.hash','uploads.name as photo_name','uploads.path')
                 ->first();

         }

         $product->colors = array();

             $product->colors = DB::table('colors as c')
                 ->where('c.product_id', $product->product_id)
                 ->orWhere('color_code', $product->color_code)
                 ->groupBy('id')
                 ->select('c.*')
                 ->get();
        // dd($product);
         return $product;
     }
    /**
     * @return array games by search
     * in header form
     */

    public function searchBy($name){
        $products = DB::table('products as p')
            ->where('p.name', 'like', '%'.$name.'%')
            ->leftJoin('uploads', 'uploads.id', '=', 'p.cover')
            ->select('p.*','uploads.hash','uploads.name as photo_name','uploads.path')
            ->orderBy('p.id', 'desc')
            ->limit(90)
            ->get();

        foreach ($products as $k){

            if(file_exists('public/uploads/product/'.$k->photo_name)){
                continue;
            }
            else{
                $img = Image::make('/var/www/drucker/storage/uploads'.$k->path);
                $img->resize(390, 300);
                $img->crop(170, 130, 40, 10);
                $img->save('public/uploads/product/'.$k->photo_name);
            }
        }

        return $products;
    }

    /**
     * @param $url
     * @return category info
     */

    public function getCategory($url){
        return DB::table('menu_lists')
                    ->where('deleted_at', NULL)
                    ->where('link', '/category/'.$url)
                    ->orderBy('sort', 'asc')
                    ->select('*')
                    ->first();
    }
    public function getProductsByCategory($link){
        $products = DB::table('products as p')
            ///->where('p.name', 'like', '%'.$name.'%')
            ->leftJoin('uploads', 'uploads.id', '=', 'p.cover')
            ->select('p.*','uploads.hash','uploads.name as photo_name','uploads.path')
            ->orderBy('p.id', 'desc')
            ->get();


        $menu =  DB::table('menu_lists')
                    ->where('deleted_at', NULL)
                    ->where('link', '/category/'.$link)
                    ->select('menu_lists.*')
                    ->orderBy('sort', 'asc')
                    ->get();

        foreach ($menu as $k){
            $k->sub = array();
            $products_sub = array();

            foreach ($products as $ken=>$key){
                foreach(json_decode($key->cat_id) as $n=>$v){
                    if($k->cat_id==$v){
                        $products_sub[$ken] = $key;
                    }

                }

                if(file_exists('public/uploads/product/'.$key->photo_name)){
                    continue;
                }
                else{
                    $img = Image::make('/var/www/drucker/storage/uploads'.$key->path);
                    $img->resize(390, 300);
                    $img->crop(170, 130, 40, 10);
                    $img->save('public/uploads/product/'.$key->photo_name);
                }
            }

        }
        return array_filter($products_sub);

    }

    public function getSubInfo($link){
        $subcategory = DB::table('subcatergories as s')
                        ->where('s.sub_link', '/products/'.$link)
                        ->select('s.*')
                        ->first();
        return $subcategory;

    }
    public function getProductsSubCategory($link){
        $products = DB::table('products as p')
            ->leftJoin('uploads', 'uploads.id', '=', 'p.cover')
            ->leftJoin('subcatergories as s', 's.id', '=', 'p.cover')
            ->select('p.*','uploads.hash','uploads.name as photo_name','uploads.path')
            ->orderBy('p.id', 'desc')
            ->get();

        $subcategory = DB::table('subcatergories as s')
                            ->where('s.sub_link', '/products/'.$link)
                            ->select('s.*')
                            ->first();


        $products_sub = array();
        if(count($products)>0){
            foreach ($products as $kk=>$k){
                    foreach(json_decode($k->cat_id) as $n=>$v){
                        if($v==$subcategory->cat_id){
                            $products_sub[$kk] = $k;
                        }
                        else{
                            continue;
                        }
                    }
                    if(file_exists('public/uploads/product/'.$k->photo_name)){
                        continue;
                    }
                    else{
                        $img = Image::make('/var/www/drucker/storage/uploads'.$k->path);
                        $img->resize(390, 300);
                        $img->crop(170, 130, 40, 10);
                        $img->save('public/uploads/product/'.$k->photo_name);
                    }


            }
            return array_filter($products_sub);
        }

        return array_filter($products_sub);
    }

    /***
     * @return choose service
     *
     */
    public function getService($id){
        return DB::table('web_site_categories')
            ->select('*')
            ->where('pid',$id)
            ->orderBy('product_model','asc')
            ->get();
    }


    
}
