<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Image;
use Cart;
use Session;
class CartSystem extends Model
{

    /**
     * @param $id
     * @return current gold item
     */
    public function getProductCustom($id,$formats_id,$printings_id,$materials_id){

        $product =  DB::table('products as p')
            ->where('p.id', $id)
            ->leftJoin('uploads', 'uploads.id', '=', 'p.cover')
            ->select('p.*','uploads.hash','uploads.name as photo_name','uploads.path')
            ->orderBy('p.id', 'desc')
            ->first();

        $product->formats = array();
            $product->formats = DB::table('fromats as f')
                ->where('f.id', $formats_id)
                ->leftJoin('uploads', 'uploads.id', '=', 'f.cover')
                ->select('f.*','uploads.hash','uploads.name as photo_name','uploads.path')
                ->first();



        $product->printings = array();
            $product->printings = DB::table('the_printings as t')
                                    ->where('t.id', $printings_id)
                                    ->leftJoin('uploads', 'uploads.id', '=', 't.name')
                                    ->select('t.*','uploads.hash','uploads.name as photo_name','uploads.path')
                                    ->first();

        $product->materials = array();
            $product->materials = DB::table('materials as m')
                                ->where('m.id', $materials_id)
                                ->leftJoin('uploads', 'uploads.id', '=', 'm.cover')
                                ->select('m.*','uploads.hash','uploads.name as photo_name','uploads.path')
                                ->first();

        // dd($product);
        return $product;
    }
    public function AddOrder($cart,$request){

        foreach(Cart::content() as $row) {
            DB::table('orders')
                ->insert(
                    [
                        'order_id'=>$request['order_id'],
                        'delivery'=>$request['delivery'],
                        'pay_type'=>$request['pay_type'],
                        'product_name'=>$row->name,
                       // 'formats'=>(isset($row->options->formats->name))? $row->options->formats->name:"".' '.(isset($row->options->formats->info))? $row->options->formats->info:"",
                        //'printings'=>(isset($row->options->printings->name))? $row->options->printings->name:"".' '.(isset($row->options->printings->info))? $row->options->printings->info:"",
                       // 'materials'=>(isset($row->options->materials->name))? $row->options->materials->name:"",
                        'total_price'=>$row->total,
                        'user_id'=>Session::get('user_info')->id,
                        'user_info'=>Session::get('user_info')->fast_name.' '.Session::get('user_info')->last_name,
                        'date_order'=>date('Y-m-d'),
                        'status'=>'Verzonden',
                        'upload_status'=>'No uploaded',
                        /*'company'=>$data_user['company'],
                        'postcode'=>$data_user['postcode'],
                        'addsess'=>$data_user['addsess'],
                        'mail' => $data_user['mail'],
                        'phone'=>$data_user['phone'],
                        'salt'=>$salt,
                        'password'=>$pass,*/
                    ]
                );

        }

        return true;
    }
    /**
     * @param $id
     * @return object of current items
     *
     */
    public function getItemProduct($id){
        $product = DB::table('product_base as pb')
            ->where('pb.id', $id)
            ->leftJoin('web_site_categories as wc', 'pb.p_game', '=', 'wc.game_id')
            ->select('pb.*')
            ->first();
        if($product){

            $product->p_price = round(((($product->p_price)*$product->p_rebate)),2);
            return $product;
        }
        else{
            return null;
        }
    }
    public function createOrder($cart_content,$transaction)
    {
        $content = '';
        $comment = '';
        $pid = 0;
        $transaction = $transaction['transaction'];


        $customer_info = Session::get('customer_info');
        //user id
        $value_user = Session::get('user_info');
        $uid = 0;
        if($value_user){
            $uid = $this->value_user->id;
        }


        DB::table('orders')
            ->insert(
                [
                    'order_number' => $transaction['order_id'],
                    'site_key' => 'gaming4ez.com',
                    'currency_tag' => $transaction['currency'],
                    'total_price' => $transaction['amount'],
                    'comment' => $comment,
                    'coupon' => '',
                    'offset' => '0',
                    'order_date' => $transaction['created'],
                    'pay_status' => 'PY',
                    'status' => 'NW',
                    'uid' => $uid,
                    'pay_type' => $transaction['pm_id'],
                    'delete_tag' => '0',
                ]
            );


        ///orders_items   WildStar - All / Stormtalon-Exiles-NA / 1 Platinum
        foreach ($cart_content as $k){
            $options = $k->options;
            $game = $this->getGoldProduct($k->id);
            $array = array();
            $array = array(
                "Character Name" => $k->options['character'],
                "Delivery method" => $k->options['deliver'],
                "Server" => $k->options['server'],
                "Comment" =>$k->options['text']
            );

            DB::table('orders_items')
                ->insert(
                    [
                        'order_number' => $transaction['order_id'],
                        'pid' => $k->id,
                        'product_type' => $k->options['product_type'],
                        'name' => $game->title.' / '.$k->options['server'].' / '.$k->options['name'],
                        'info' => $k->text,
                        'price' => $k->price,
                        'num' => $k->qty,
                        'game' => $game->title,
                        'game_server' => $k->options['server'],
                        'input' => serialize($array),
                        'gold_num' => '0',
                    ]
                );
        }

        if($this->value_user){
            $uid = $this->value_user->id;

            DB::table('orders_paypal')
                ->insert(
                    [
                        'order_number' => $transaction['order_id'],
                        'business' => 'hldstore86@gmail.com',
                        'receiver_email' => 'hldstore86@gmail.com',
                        'receiver_id' => 'BTAD6T6BT67RE',
                        'verify_sign' => 'payssion',
                        'payer_id' => 'payssion',
                        'txn_id' => 'payssion',
                        'payment_status' => $transaction['state'],
                        'payer_email' => $this->value_user->mail,
                        'payment_amount' => $transaction['amount'],
                        'payment_currency' => $transaction['currency'],
                        'payment_fee' => roudn((($transaction['amount']/100)*3.6),2),
                        'payment_date' => date('H:i:s Y-m-d',$transaction['created']),
                        'memo'=>'Write your message here',
                        'address_country'=>$this->value_user->country,
                        'address_city'=>$this->value_user->country,
                        'address_street'=>$this->value_user->country,
                        'address_zip'=>$this->value_user->country,
                        'contact_phone'=>$this->value_user->country,
                        'address_status'=>$this->value_user->country,
                        'first_name'=>$this->value_user->first_name,
                        'last_name'=>$this->value_user->last_name,
                        'payer_status' => $transaction['state'],
                        'pay_type' => $transaction['pm_id'],
                    ]
                );
        }
        if($customer_info){
            DB::table('orders_paypal')
                ->insert(
                    [
                        'order_number' => $transaction['order_id'],
                        'business' => 'hldstore86@gmail.com',
                        'receiver_email' => 'hldstore86@gmail.com',
                        'receiver_id' => 'BTAD6T6BT67RE',
                        'verify_sign' => 'payssion',
                        'payer_id' => 'payssion',
                        'txn_id' => 'payssion',
                        'payment_status' => $transaction['state'],
                        'payer_email' => $customer_info['email'],
                        'payment_amount' => $transaction['amount'],
                        'payment_currency' => $transaction['currency'],
                        'payment_fee' => round((($transaction['amount']/100)*3.6),2),
                        'payment_date' => date('H:i:s Y-m-d',$transaction['created']),
                        'memo'=>'Write your message here',
                        'address_country'=>$customer_info['country'],
                        'address_city'=>$customer_info['country'],
                        'address_street'=>$customer_info['country'],
                        'address_zip'=>$customer_info['country'],
                        'contact_phone'=>$customer_info['phone'],
                        'address_status'=>$customer_info['country'],
                        'first_name'=>$customer_info['name'],
                        'last_name'=>$customer_info['last_name'],
                        'payer_status' => $transaction['state'],
                        'pay_type' => $transaction['pm_id'],
                    ]
                );
        }

        Cart::destroy();

    }
}
