<?php

/* ================== Homepage ================== */
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');

	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::get(config('laraadmin.adminRoute') . '/upload_dt_ajax', 'LA\UploadsController@dtajax');

	/* ================== Menu_lists ================== */
	Route::resource(config('laraadmin.adminRoute') . '/menu_lists', 'LA\Menu_listsController');
	Route::get(config('laraadmin.adminRoute') . '/menu_list_dt_ajax', 'LA\Menu_listsController@dtajax');

	/* ================== Sliders ================== */
	Route::resource(config('laraadmin.adminRoute') . '/sliders', 'LA\SlidersController');
	Route::get(config('laraadmin.adminRoute') . '/slider_dt_ajax', 'LA\SlidersController@dtajax');


	/* ================== Why_chooses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/why_chooses', 'LA\Why_choosesController');
	Route::get(config('laraadmin.adminRoute') . '/why_choose_dt_ajax', 'LA\Why_choosesController@dtajax');

	/* ================== Message_contacts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/message_contacts', 'LA\Message_contactsController');
	Route::get(config('laraadmin.adminRoute') . '/message_contact_dt_ajax', 'LA\Message_contactsController@dtajax');


	/* ================== Faqs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/faqs', 'LA\FaqsController');
	Route::get(config('laraadmin.adminRoute') . '/faq_dt_ajax', 'LA\FaqsController@dtajax');

	/* ================== Pages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/pages', 'LA\PagesController');
	Route::get(config('laraadmin.adminRoute') . '/page_dt_ajax', 'LA\PagesController@dtajax');

	/* ================== Pages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/web_customers', 'LA\Web_customersController');
	Route::get(config('laraadmin.adminRoute') . '/web_customers_dt_ajax', 'LA\Web_customersController@dtajax');




	/* ================== Coupons ================== */
	Route::resource(config('laraadmin.adminRoute') . '/coupons', 'LA\CouponsController');
	Route::get(config('laraadmin.adminRoute') . '/coupon_dt_ajax', 'LA\CouponsController@dtajax');

	/* ================== Account_levels ================== */
	Route::resource(config('laraadmin.adminRoute') . '/account_levels', 'LA\Account_levelsController');
	Route::get(config('laraadmin.adminRoute') . '/account_level_dt_ajax', 'LA\Account_levelsController@dtajax');


	/* ================== User_coupons ================== */
	Route::resource(config('laraadmin.adminRoute') . '/user_coupons', 'LA\User_couponsController');
	Route::get(config('laraadmin.adminRoute') . '/user_coupon_dt_ajax', 'LA\User_couponsController@dtajax');






	/* ================== Interest_rates ================== */
	Route::resource(config('laraadmin.adminRoute') . '/interest_rates', 'LA\Interest_ratesController');
	Route::get(config('laraadmin.adminRoute') . '/interest_rate_dt_ajax', 'LA\Interest_ratesController@dtajax');


	/* ================== Web_guestbooks ================== */
	Route::resource(config('laraadmin.adminRoute') . '/web_guestbooks', 'LA\Web_guestbooksController');
	Route::get(config('laraadmin.adminRoute') . '/web_guestbook_dt_ajax', 'LA\Web_guestbooksController@dtajax');


	/* ================== Subcatergories ================== */
	Route::resource(config('laraadmin.adminRoute') . '/subcatergories', 'LA\SubcatergoriesController');
	Route::get(config('laraadmin.adminRoute') . '/subcatergory_dt_ajax', 'LA\SubcatergoriesController@dtajax');

	/* ================== Products ================== */
	Route::resource(config('laraadmin.adminRoute') . '/products', 'LA\ProductsController');
	Route::get(config('laraadmin.adminRoute') . '/product_dt_ajax', 'LA\ProductsController@dtajax');

	/* ================== Banners ================== */
	Route::resource(config('laraadmin.adminRoute') . '/banners', 'LA\BannersController');
	Route::get(config('laraadmin.adminRoute') . '/banner_dt_ajax', 'LA\BannersController@dtajax');

	/* ================== Fromats ================== */
	Route::resource(config('laraadmin.adminRoute') . '/fromats', 'LA\FromatsController');
	Route::get(config('laraadmin.adminRoute') . '/fromat_dt_ajax', 'LA\FromatsController@dtajax');

	/* ================== The_printings ================== */
	Route::resource(config('laraadmin.adminRoute') . '/the_printings', 'LA\The_printingsController');
	Route::get(config('laraadmin.adminRoute') . '/the_printing_dt_ajax', 'LA\The_printingsController@dtajax');

	/* ================== Customers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/customers', 'LA\CustomersController');
	Route::get(config('laraadmin.adminRoute') . '/customer_dt_ajax', 'LA\CustomersController@dtajax');

	/* ================== Materials ================== */
	Route::resource(config('laraadmin.adminRoute') . '/materials', 'LA\MaterialsController');
	Route::get(config('laraadmin.adminRoute') . '/material_dt_ajax', 'LA\MaterialsController@dtajax');

	/* ================== Orders ================== */
	Route::resource(config('laraadmin.adminRoute') . '/orders', 'LA\OrdersController');
	Route::get(config('laraadmin.adminRoute') . '/order_dt_ajax', 'LA\OrdersController@dtajax');

	/* ================== Product_weights ================== */
	Route::resource(config('laraadmin.adminRoute') . '/product_weights', 'LA\Product_weightsController');
	Route::get(config('laraadmin.adminRoute') . '/product_weight_dt_ajax', 'LA\Product_weightsController@dtajax');

	/* ================== Number_of_pages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/number_of_pages', 'LA\Number_of_pagesController');
	Route::get(config('laraadmin.adminRoute') . '/number_of_page_dt_ajax', 'LA\Number_of_pagesController@dtajax');

	/* ================== Colors ================== */
	Route::resource(config('laraadmin.adminRoute') . '/colors', 'LA\ColorsController');
	Route::get(config('laraadmin.adminRoute') . '/color_dt_ajax', 'LA\ColorsController@dtajax');
});
