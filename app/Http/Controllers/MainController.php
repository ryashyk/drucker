<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Image;
use App\Models\Main;
use App\Product;
use Session;
// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;

class MainController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
        
        //Image::configure(array('driver' => 'imagick'));
        view()->share('title', 'MultiMedia');
        $main = new Main();
        return view('home',
                ['slider'=> $main->slides(),
                    'why_choose'=>$main->whyChoose(),
                    ///'reviews'=>$main->reviews(),
                    ///'news_home'=>$main->getNews(),
                    'popular'=>$main->getPopularProducts(),
                    'recommended' =>$main->getRecommendedProducts(),
                ]);
    }
    public function contact(){
        view()->share('title', 'Contacts');
        return view('contact');
    }

    public function chat(){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        view()->share('title', 'Chat beschikbaar');
        return view('chat');
    }

    /**
     * @return exit to profile
     */
    public function logout_user(){
        Session::forget('user_info');
       // Session::flush();
        session_unset();
        return redirect('/');
    }


    /**
     * @return page search
     * @params request data from form
     */
    public function search(Request $request){
        $products = new Product();
        view()->share('title', 'Zoek een product - '.$request->get('name'));
        return view('search',['products'=>$products->searchBy($request->get('name')),'name'=>$request->get('name')]);
    }


    public function faq(){
        $faq = DB::table('faqs')
            ->where('deleted_at', NULL)
            ->orderBy('order_item', 'asc')
            ->select('*')
            ->get();
        view()->share('title', 'Frequently Asked Questions');
        return view('faq',['faq'=>$faq]);
    }
    public function page($url){
        $content = DB::table('pages')
            ->where('deleted_at', NULL)
            ->where('link',$url)
            ->select('*')
            ->first();

        if($content==null){
            return response(view('errors.404'), 404);
        }

        view()->share('title', $content->name);
        return view('page',
            ['page'=>$content]);
    }
}
