<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;


use App\Product;
use App\Models\Main;

use DB;
use Image;

class ProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($url=''){
        $product = new Product();
        $main = new Main();
        $category = $product->getCategory($url);
        if($category!=null){

            view()->share('title', $category->name);
            return view('products_cat',
                [
                    'products'=>$product->getProductsByCategory($url),
                    'category'=>$category,
                ]);
        }else{
            return response(view('errors.404'), 404);
        }

    }

    public function product($id){
        $product = new Product();
        $main = new Main();
        $id = (int)$id;
        $product_data = $product->getProduct($id);
        if($product_data!=null){

            view()->share('title', $product_data->name);
            return view('product',
                [
                    'product_data'=>$product_data,
                ]);
            
        }
        else{
            return response(view('errors.404'), 404);
        }
        
    }

    public function products($url){
        $product = new Product();
        $main = new Main();
        $subcat = $product->getSubInfo($url);
        if(count($subcat)>0){
            view()->share('title', $subcat->sub_name);
            return view('products',
                [
                    'products'=>$product->getProductsSubCategory($url),
                    'subcat'=>$subcat,
                ]);
        }
        else{
            return response(view('errors.404'), 404);
        }


    }




}
