<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\User;
use App\CartSystem;


use Session;
use Cart;



class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function parseAction(Request $request)
    {

            switch($request->get('action')) {
                
                case 'contact_us':

                    $validator = Validator::make($request->all(), [
                        'email' => 'required|max:255|email',
                        'text' => 'required',
                    ]);

                    if ($validator->fails()) {
                        $error = $validator->errors();

                        return response()->json(array('status'=>false,'error'=> $error));
                    }

                    DB::table('message_contacts')->insert(
                        ['email' => $request->get('email'), 'text' => $request->get('text')]
                    );


                    return response()->json(array('status'=>true,'msg'=> 'The data has been successfully sent'), 200);

                case 'registration_user':

                    $validator = Validator::make($request->all(), [
                        'mail' => 'required|max:255|email|min:2',
                        'password' => 'required|max:32|min:6',
                        'password_confirmation' => 'required',
                        'phone' => 'required|integer|max:999999999999|min:4',
                        'fast_name' => 'required|max:32|min:1',
                        'last_name' => 'required|max:32|min:1',
                        'country' => 'required|max:92|min:1',
                        'company' => 'required|max:92|min:1',
                        'postcode' => 'required|max:92|min:1',
                        'sex' => 'required|max:92|min:1',
                        'addsess' => 'required|max:192|min:1',
                    ]);

                    if ($validator->fails()) {
                        $error = $validator->errors();

                        return response()->json(array('status'=>false,'error'=> $error));
                    }
                    $user = new User();
                    $approved_user = $user->checkEmail($request->get('email'));
                    /**
                     * if email exist
                     * */
                    if($approved_user){
                        return response()->json(array('status'=>false,'msg'=> 'Such a user already exists'));
                    }else{
                        /**
                         * create user if user is not exist
                        */
                         $user_data = $user->createUser($request->all());
                        
                        return response()->json(array('status'=>true));
                    }
                case 'login_user':

                    $validator = Validator::make($request->all(), [
                        'email' => 'required|max:255|email',
                        'password' => 'required',
                    ]);

                    if ($validator->fails()) {
                        $error = $validator->errors();

                        return response()->json(array('status'=>false,'error'=> $error));
                    }
                    $user = new User();
                    $approved_user = $user->checkEmail($request->get('email'));
                    /**
                     * if email exist
                     * */
                    if($approved_user){
                        $approved_user = $approved_user['0'];

                        /**
                         * if password good then
                         * login user
                         * */
                        if($user->checkPassword($approved_user->mail,$request->get('password'),$approved_user->salt)){

                             Session::put('user_info', $approved_user);
                             return response()->json(array('status'=>true), 200);
                        }
                        else{
                            return response()->json(array('status'=>false,'msg'=> 'Login or password are incorrect'));
                        }

                    }else{
                        return response()->json(array('status'=>false,'msg'=> 'Login or password are incorrect'));
                    }




                case 'change_user_data':
                    $validator = Validator::make($request->all(), [
                        'fast_name' => 'required',
                        'last_name' => 'required',
                        'country' => 'required',
                        'phone' => 'required',
                    ]);

                    if ($validator->fails()) {
                        $error = $validator->errors();

                        return response()->json(array('status'=>false,'error'=> $error));
                    }
                    else{
                        $user = new User();
                        $change_data = $user->changeUserData($request->all());
                        /**
                         * if data success change
                         * */
                        if($change_data){
                                return response()->json(array('status'=>true,'msg'=>'Data change success'), 200);
                        }
                        else{
                            return response()->json(array('status'=>false,'msg'=>'Data not change'), 200);
                        }
                    }


                case 'add_product_photo':
                    //print_r($request->all());
                    $validator = Validator::make($request->all(), [
                        'photo' => 'required',
                        'action' => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = $validator->errors();

                        return response()->json(array('status'=>false,'error'=> $error));
                    }
                    else{
                        $user = new User();
                        $change_data = $user->uploadProductPhoto($request->all());
                        /**
                         * if data success change photo
                         * */
                        if($change_data){
                            return response()->json(array('status'=>true,'msg'=>'Photo has changed','photo'=>$change_data), 200);
                        }
                        else{
                            return response()->json(array('status'=>false,'msg'=>'Error photo no changed'), 200);
                        }
                    }

                case 'coupon_active':
                    //print_r($request->all());
                    $validator = Validator::make($request->all(), [
                        'name' => 'required',
                        'action' => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = $validator->errors();

                        return response()->json(array('status'=>false,'error'=> $error));
                    }
                    else{
                        $user = new User();
                        $change_data = $user->activateCouponByCode($request->get('name'));
                        /**
                         * if data success change photo
                         * */
                        if($change_data){
                            return response()->json(array('status'=>true,'msg'=>'Coupon has add','coupon'=>$change_data), 200);
                        }
                        else{
                            return response()->json(array('status'=>false,'msg'=>'Error coupon no exist'), 200);
                        }
                    }

                case 'add_order':

                    $cart_system = new CartSystem();
                    $cart_system->AddOrder(Cart::content(),$request->all());

                        Cart::destroy();
                        return response()->json(array('status'=>true));

                case 'access_checkout':
                    $validator = Validator::make($request->all(), [
                        'email' => 'required|max:255|email|min:2',
                        'phone' => 'required|integer|max:999999999999|min:4',
                        'name' => 'required|max:32|min:1',
                        'last_name' => 'required|max:32|min:1',
                        'country' => 'required|max:32|min:1',
                    ]);

                    if ($validator->fails()) {
                        $error = $validator->errors();

                        return response()->json(array('status'=>false,'error'=> $error));
                    }

                    $user = new User();
                    /**
                     * set session user info
                     * */
                        $customer_info = array(
                                                'email'=>$request->get('email'),
                                                'phone'=>$request->get('phone'),
                                                'name'=>$request->get('name'),
                                                'last_name'=>$request->get('last_name'),
                                                'country'=>$request->get('country')
                                        );
                        Session::put('customer_info', $customer_info);

                        return response()->json(array('status'=>true));


                case 'add_to_cart':

                    $cart_system = new CartSystem();
                    //Cart::destroy();
                    $info_cart = $cart_system->getProductCustom($request->get('id'),$request->get('formats'),$request->get('printings'),$request->get('materials'));
                    if($info_cart){
                            Cart::add($info_cart->id, $info_cart->name, 1, $info_cart->price, 0.00, array('formats' => $info_cart->formats,'printings'=>$info_cart->printings,'materials'=>$info_cart->materials));
                            return response()->json(array('status'=>true,'total'=> Cart::count(),'content'=>Cart::content(),'price'=>Cart::total()));
                    }
                    else{
                        return response()->json(array('status'=>false,'msg'=> 'Product incorrect'));
                    }



                    break;
            }

    }
}


