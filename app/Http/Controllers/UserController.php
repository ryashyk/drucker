<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;

use DB;
use Image;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index(Request $request){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        if($request->get('id')){
            $orders = $user->getUserOrdersDataID($request->get('id'));
        }
        else{
            $orders = $user->getUserOrdersData();
        }
        
        
        
        return view('user.profile-data',
                        [
                            'user_data'=>$user->getUserData($this->value_user->mail),
                            'list_country'=> $user->getCountry(),
                            'orders'=> $orders,
                        ]);
    }
    /**
     * return profile coupons
    */
    public function profile_account(){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        return view('user.profile_account',
            ['user_data'=>$user->getUserData($this->value_user->mail),
            ]);
    }

    /**
     * @param $id
     * @return full description order
     */
    public function profile_order($id){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        $order = $user->getUserOrderId($id);
        if($order==null){
            return response(view('errors.404'), 404);
        }
        return view('user.profile_order', [
                'order'=>$order
            ]);
    }


    /**
     * @param $id
     * @return upload images for order
     */
    public function profile_order_upload($id){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        $order = $user->getUserOrderId($id);
        if($order==null){
            return response(view('errors.404'), 404);
        }
        return view('user.profile_order_upload', [
            'order'=>$order
        ]);
    }

    /**
     * @return account level
    */
    public function profile_klachten(){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        return view('user.profile_klachten',
            [
                //'coupons'=>$user->getByCouponsList(),
                //'user_data'=>$user->getUserData($this->value_user->mail),
                //'account_levels'=>$user->getAccountLevels(),
            ]);
    }

    /**
     * @return forms with user
     * can change email or password
     */
    public function profile_security(){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        return view('user.profile_security');
    }



    /**
     * @return inbox list
     * can read self message
     */
    public function profile_inbox(){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        return view('user.profile_inbox');
    }
    /**
     * @return orders list
     * can read state order
     */
    public function profile_orders(){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        return view('user.profile_orders',['orders'=> $user->getUserOrdersData(),]);
    }
    /***
     * @return account credits
     */
    public function profile_credits(){
        if($this->value_user==null){
            return redirect('/login_user');
        }
        $user = new User();
        return view('user.profile_credits',['user_data'=>$user->getUserData($this->value_user->mail),]);
    }
    public function login(){
        if($this->value_user){
            return redirect('/profile_user');
        }
        return view('user.login_user');
    }
    
    public function registration_user(){
        $user = new User();
        if($this->value_user){
            return redirect('/profile_user');
        }
        return view('user.registration_user',['list_country'=> $user->getCountry()]);
    }
}
