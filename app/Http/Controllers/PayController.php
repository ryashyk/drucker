<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Lib\PayssionClient;
use Illuminate\Http\Request;

/**
 * Class PayController
 * @package App\Http\Controllers
 */
class PayController extends Controller
{
    public function __construct()
    {

    }
    public function index()
    {
        $payssion = new PayssionClient('90effad0fd74ce41', 'f34b08d702cb6f963f38d551dc2d3c4a');
//please uncomment the following if you use sandbox api_key
//$payssion = new PayssionClient('your api key', 'your secretkey', false);
        $response = null;
        try {
            $response = $payssion->create(array(
                'amount' => 1,
                'currency' => 'USD',
                'pm_id' => 'hsbc_br',
                'description' => 'order description',
                'order_id' => 'your order id',          //your order id
                'return_url' => 'your return url'   //optional, the return url after payments (for both of paid and non-paid)
            ));
        } catch (Exception $e) {
            //handle exception
            echo "Exception: " . $e->getMessage();
        }
        if ($payssion->isSuccess()) {
            //handle success
            $todo = $response['todo'];
            if ($todo) {
                $todo_list = explode('|', $todo);
                if (in_array("redirect", $todo_list)) {
                    //redirect the users to the redirect url or send the url by email
                    $paylink = $response['redirect_url'];
                    echo $paylink;
                }
            } else {
                //just in case, should not be here
            }
        } else {
            //handle failed
        }
    }


    public function details(){
        $payssion = new PayssionClient('your api key', 'your secretkey');
//please uncomment the following if you use sandbox api_key
//$payssion = new PayssionClient('your api key', 'your secretkey', false);
        $response = null;
        try {
            $response = $payssion->getDetails(array(
                'order_id' => 'your order id',  //your order id
            ));
        } catch (Exception $e) {
            //handle exception
            echo "Exception: " . $e->getMessage();
        }
        if ($payssion->isSuccess()) {
            //handle success
            $transaction = $response['transaction'];
            $pm_id = $transaction['pm_id'];
            $amount = $transaction['amount'];
            $currency = $transaction['currency'];
            $order_id = $transaction['order_id'];
            $state = $transaction['state'];
        } else {
            //handle failed
        }
    }
}