<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Image;
use App\Models\Main;
use App\Product;
use Session;
// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;

class TestController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    public function index(){

        $xml = 'html/categories.xml';
        if(file_exists($xml)){
            $fileContents= file_get_contents($xml);

            $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);

            $fileContents = trim(str_replace('"', "'", $fileContents));

            $simpleXml = simplexml_load_string($fileContents);

            $json = ($simpleXml);

            foreach ($json->Categories->Category as $k){
                /*if($k->Level==1){
                    var_dump($k);

                    DB::table('menu_lists')
                        ->insert(
                            [
                                'name'=>$k->NL_Description,
                                'link'=>'/category/'.trim(strtolower($k->NL_Description)),
                                'active'=>1,
                                'sort'=>$k->Category_Id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'cat_id'=>$k->Category_Id,
                            ]
                        );

                }*/
                /*if($k->Level==2){
                    var_dump($k);
                    sleep(1);
                    DB::table('subcatergories')
                        ->insert(
                            [
                                'sub_name'=>$k->NL_Description,
                                'sub_link'=>'/products/'.str_slug($k->NL_Description),
                                'status'=>1,
                                'order'=>$k->Category_Id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'cat_id'=>$k->Category_Id,
                                'cat_name'=>$k->Parent
                            ]
                        );
                }*/
            }


        }

    }


    public function index2(){

        $xml = 'html/products.xml';
        if(file_exists($xml)){
            $fileContents= file_get_contents($xml);

            $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);

            $fileContents = trim(str_replace('"', "'", $fileContents));

            $simpleXml = simplexml_load_string($fileContents);

            $json = ($simpleXml);
            $i = 0;
            foreach ($json->Products->Product as $k){
                $parse_p = parse_url($k->Image_Together);
                $explode_image = explode('/',$parse_p['path']);
                $last_element = (count($explode_image)-1);
                $photo_name = $explode_image[$last_element];

                //dd(json_encode($k->Categories->Category_Id));
                $count_cat = (count($k->Categories->Category_Id));
                $counter = '0';
                $strn = '[';
                foreach ($k->Categories->Category_Id as $i=>$v){
                    $counter = $counter+1;
                    if($count_cat==$counter){
                        $strn = $strn.'"'.$v.'"]';
                    }
                    else{
                        $strn = $strn.'"'.$v.'",';
                    }
                }
                $url = $k->Image_Together;


                /*if($k->Prices->Price->Price[0]!=null){


                    $pid = DB::table('products as p')
                            ///->where('p.name', 'like', '%'.$name.'%')
                            ->select('p.*')
                            ->orderBy('p.id', 'desc')
                            ->where('product_id',$k->Product_Id)
                            ->first();

                    if($pid==null){
                        $img = 'storage/uploads/'.$photo_name;
                        file_put_contents($img, file_get_contents($url));

                        $str = $photo_name;
                        $time_path = '/'.$str;
                        $photo =  DB::table('uploads')
                            ->insertGetId([
                                'name'=>$str,
                                'path'=>$time_path,
                                'extension'=>'jpg',
                                'user_id'=>'1',
                                'hash'=>'rbn7couy0fp4u5rtvzsm',
                                'public'=>'1',
                                'created_at'=>date('Y-m-d'),
                                'updated_at'=>date('Y-m-d'),
                            ]);

                        DB::table('products')
                            ->insert(
                                [
                                    'name'=>$k->NL_Name,
                                    'product_id'=>$k->Product_Id,
                                    'color_code'=>$k->Color_Code,
                                    'price'=>$k->Prices->Price->Price[0],
                                    'cat_id'=>$strn,
                                    'info'=>$k->NL_Description,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'cover'=>$photo,
                                    'recommended'=>1,
                                    'popular'=>1

                                ]
                            );
                    }else{
                        continue;
                    }
                    sleep(1);
                }*/




            }


        }

    }






    public function index3()
    {

        $xml = 'html/print.xml';
        if (file_exists($xml)) {
            $fileContents = file_get_contents($xml);

            $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);

            $fileContents = trim(str_replace('"', "'", $fileContents));

            $simpleXml = simplexml_load_string($fileContents);

            $json = ($simpleXml);
            $i = 0;


            foreach ($json->Products->Product as $k) {


                dd($k->Positions->Position);
                exit;

                DB::table('the_printings')
                    ->insert(
                        [
                            'name' => '103',
                            'product_id' => $k->Product_Code,
                            'price' => '0',
                            'cover'=>$k->NL_Sub_Article_Title,
                            'info' => $k->NL_Print_Method_Title,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),

                        ]
                    );

                sleep(1);
            }

        }
    }




    public function index4()
    {

        $xml = 'html/colors.xml';
        if (file_exists($xml)) {
            $fileContents = file_get_contents($xml);

            $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);

            $fileContents = trim(str_replace('"', "'", $fileContents));

            $simpleXml = simplexml_load_string($fileContents);

            $json = ($simpleXml);
            $i = 0;


            foreach ($json->Colors->Color as $k) {

/*
                DB::table('colors')
                    ->insert(
                        [
                            'name' => $k->NL_Title,
                            'product_id' => $k->Product_Id,
                            'hex'=>$k->RGB,
                            'color_code' => $k->Color_Code,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),

                        ]
                    );
              */
                sleep(1);
            }

        }
    }



}
