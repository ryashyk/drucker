<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Models\Main;

class ReviewsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        view()->share('title', 'Reviews');
        $reviews = new Main();

        return view('reviews', ['reviews'=>$reviews->getReviews()]);
    }
}
