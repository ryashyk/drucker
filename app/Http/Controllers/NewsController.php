<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon;
use App\User;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class NewsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $page = Input::get('page');
        if($page==null){
            $page = 0;
        }
        view()->share('title', 'News');
        $news = DB::table('web_informations')
            ->where('info_state', 1)
            ->where('info_website', 43)
            ->select('web_informations.*')
            ->orderBy('id', 'desc')
            ->skip($page)
            ->take(6)
            ->get();

        $pagination = DB::table('web_informations')
                        ->where('info_state', 1)
                        ->where('info_website', 43)
                        ->paginate(6);
        return view('news',
            ['news'=>$news,'pagination'=>$pagination]);
    }

    /*
     * get current news page
     * */
    public function news_is($id){
        //Auth::login();
        //Auth::attempt(array('email' => 'zhelinscky@gmail.com', 'password' => 'demodemo'), true);

           // $user = new User();
           // $user->name = "admin@webcapitan.com";
           // $user->email = "zhelinscky@gmail.com";
           // $user = DB::table('web_customers')->where('mail' , 'vseach@163.com')->first();
        //dd($user);
          //  Auth::login($user);

            //return redirect()->intended('/profile');

        $content = DB::table('web_informations')
            ->where('info_state', 1)
            ->where('info_website', 43)
            ->where('id',$id)
            ->select('web_informations.*')
            ->first();

        if($content==null){
            return response(view('errors.404'), 404);
        }

        view()->share('title', $content->info_title);

        return view('text_page',
            ['page'=>$content]);
    }
}
