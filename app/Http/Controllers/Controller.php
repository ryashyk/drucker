<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use View;
use App\Models\Main;
use Session;
use Cart;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    protected $value_user;
    public $customer_info=null;

    public function __construct()
    {
        $local = new Main();
        View::share ('title', 'MultiMedia');
        View::share ('home_menu', $local->getMenu());
       // View::share ('game_menu', $local->getGameMenu());
        View::share ('cart_items',  Cart::count());
        View::share ('total_price',  Cart::total());
        View::share ('user',  Session::get('user_info'));
        View::share ('customer_info',  Session::get('customer_info'));
        View::share ('currency',  '$');
        /*
         * auth user info
         * */
         $this->value_user = Session::get('user_info');


        $this->customer_info = Session::get('customer_info');
    }


}
