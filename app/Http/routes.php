<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', [
    'uses' => 'MainController@index'
]);
Route::post('/', [
    'uses' => 'MainController@index'
]);

//current services of games
Route::get('test', [
    'uses' => 'TestController@index'
]);

//current services of games
Route::get('test2', [
    'uses' => 'TestController@index2'
]);
Route::get('test3', [
    'uses' => 'TestController@index3'
]);
Route::get('test4', [
    'uses' => 'TestController@index4'
]);


//reviews
Route::get('reviews', [
    'uses' => 'ReviewsController@index'
]);


Route::get('profile_order/{id}', [
    'uses' => 'UserController@profile_order'
]);
Route::get('profile_order_upload/{id}', [
    'uses' => 'UserController@profile_order_upload'
]);



Route::get('logout_user', [
    'uses' => 'MainController@logout_user'
]);
Route::get('chat', [
    'uses' => 'MainController@chat'
]);

Route::get('news', [
    'uses' => 'NewsController@index'
]);
//contact
Route::get('contact', [
    'uses' => 'MainController@contact'
]);

//contact
Route::get('cart', [
    'uses' => 'CartController@index'
]);

//contact
Route::get('clear_cart', [
    'uses' => 'CartController@clear_cart'
]);

//succes payment

Route::get('payssion_payment/{id}', [
    'uses' => 'CartController@successPayment'
]);
//succes payment

Route::get('paypal_cancel', [
    'uses' => 'CartController@paypalCancel'
]);
//
Route::post('paypal_success', [
    'uses' => 'CartController@paypalSuccess'
]);
//contact
Route::get('faq', [
    'uses' => 'MainController@faq'
]);

/**
 * routes of game pages
 */

Route::get('games', [
    'uses' => 'GameController@index'
]);




Route::get('product-one/id-{id}', [
    'uses' => 'ProductController@product'
]);

Route::get('products/{url}', [
    'uses' => 'ProductController@products'
]);


Route::get('category/{link}', [
    'uses' => 'ProductController@index'
]);
Route::get('game_service/{url}', [
    'uses' => 'GameController@game_service'
]);

Route::post('news/news_is-{id}', 'NewsController@news_is');
Route::get('news/news_is-{id}', [
    'uses' => 'NewsController@news_is'
]);

Route::post('page/{url}', 'MainController@page');
Route::get('page/{url}', [
    'uses' => 'MainController@page'
]);

Route::get('login_user', [
    'uses' => 'UserController@login'
]);
Route::get('registration_user', [
    'uses' => 'UserController@registration_user'
]);

Route::get('profile_user', [
    'uses' => 'UserController@index'
]);
Route::post('profile_user', [
    'uses' => 'UserController@index'
]);


Route::get('profile_user', [
    'uses' => 'UserController@index'
]);


Route::get('profile_inbox', [
    'uses' => 'UserController@profile_inbox'
]);
Route::get('profile_orders', [
    'uses' => 'UserController@profile_orders'
]);
Route::get('profile_account', [
    'uses' => 'UserController@profile_account'
]);

/**
 * search form
 */
Route::post('search', [
    'uses' => 'MainController@search'
]);


Route::get('profile_klachten', [
    'uses' => 'UserController@profile_klachten'
]);


Route::get('profile_security', [
    'uses' => 'UserController@profile_security'
]);
Route::get('profile_credits', [
    'uses' => 'UserController@profile_credits'
]);
//all ajax
Route::post('ajax', ['as' => 'ajax', 'uses' => 'AjaxController@parseAction']);
//ajax choose payment method
Route::post('choose_payment', ['as' => 'choose_payment', 'uses' => 'CartController@choose_payment']);


Route::get('profile', function () {
    // Only authenticated users may enter...
})->middleware('auth');
/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';