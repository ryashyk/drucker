<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Image;
class Main extends Model
{
    public function getMenu(){
        $menu =  DB::table('menu_lists as ml')
                    ->where('ml.deleted_at', NULL)
                    ->orderBy('sort', 'asc')
                    ->get();
        foreach ($menu as $k){
            $k->sub = array();

            $k->sub =  DB::table('subcatergories')
                            ->where('deleted_at', NULL)
                            ->where('cat_name',$k->cat_id)
                            ->orderBy('order', 'asc')
                            ->get();
        }
        //dd($menu);
        return $menu;
    }

    public function slides(){
        return DB::table('sliders')
                    ->where('sliders.deleted_at', NULL)
                    ->leftJoin('uploads', 'uploads.id', '=', 'sliders.file')
                    ->select('sliders.*','uploads.path as photo_path','uploads.name as photo_name')
                    ->get();
    }
    
    public function whyChoose(){
        return DB::table('why_chooses')
                    ->where('why_chooses.deleted_at', NULL)
                    ->leftJoin('uploads', 'uploads.id', '=', 'why_chooses.cover')
                    ->select('why_chooses.*','uploads.path as photo_hash','uploads.name as photo_name')
                    ->get();
    }

    public function reviews(){
         return   DB::table('web_guestbook')
                ->where('web_guestbook.state', 1)
                ->where('web_guestbook.site_key','gaming4ez.com')
                //->leftJoin('uploads', 'uploads.id', '=', 'why_chooses.cover')
                ->select('web_guestbook.content','web_guestbook.username')
                ->limit(4)
                ->orderBy('id','desc')
            ->get();
    }
    public function getNews(){
        return DB::table('web_informations')
                ->where('info_state', 1)
                ->where('info_website', 43)
                ->select('web_informations.*')
                ->orderBy('id', 'desc')
                ->limit(4)
                ->get();
    }
    /**
     * return hot games
     * and create game image
     */
    public function getRecommendedProducts(){
        $products = DB::table('products as p')
            ->where('p.recommended', 1)
            ->leftJoin('uploads', 'uploads.id', '=', 'p.cover')
            ->select('p.*','uploads.hash','uploads.name as photo_name','uploads.path')
            ->orderBy('p.id', 'desc')
            ->limit(30)
            ->get();

        foreach ($products as $k){

            if(file_exists('public/uploads/product/'.$k->photo_name)){
                continue;
            }
            else{
                $img = Image::make('/var/www/drucker/storage/uploads'.$k->path);
                $img->resize(290, 200);
                $img->crop(170, 130, 10, 10);
                $img->save('public/uploads/product/'.$k->photo_name);
            }
        }

        return $products;
    }

    public function getPopularProducts(){
        $products = DB::table('products as p')
            ->where('p.popular', 1)
            ->leftJoin('uploads', 'uploads.id', '=', 'p.cover')
            ->select('p.*','uploads.hash','uploads.name as photo_name','uploads.path')
            ->orderBy('p.id', 'desc')
            ->limit(30)
            ->get();

        foreach ($products as $k){

            if(file_exists('public/uploads/product/'.$k->photo_name)){
                continue;
            }
            else{
                $img = Image::make('/var/www/drucker/storage/uploads'.$k->path);
                $img->resize(290, 200);
                $img->crop(170, 130, 10, 10);
                $img->save('public/uploads/product/'.$k->photo_name);
            }
        }

        return $products;
    }

    public function getReviews(){
        return DB::table('web_guestbook')
                    ->where('state', 1)
                    ->select('content','username','re_content','re_username','id')
                    ->get();
    }
}
