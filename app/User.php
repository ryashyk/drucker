<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use DB;
use Session;
use Image;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    // use SoftDeletes;
    use EntrustUserTrait;


    protected $table = 'users';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'name', 'email', 'password', "role", "context_id", "type"
	];
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = [
		'password', 'remember_token',
    ];
    
    // protected $dates = ['deleted_at'];

    /**
     * @return mixed
     */
    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }

    /**
     * @return true if email exist
     * from table web_customers
     */
    public function checkEmail($email){
        return DB::table('customers')
                    ->where('mail',$email)
                    ->select('id','mail','fast_name','last_name','salt')
                    ->get();
    }

    /**
     * @return true if salt plus password has found
     *
     * */
    public function checkPassword($mail,$pass,$salt){
        $current_pass = md5($salt . $pass);
        return DB::table('customers')
            ->where('mail',$mail)
            ->where('password',$current_pass)
            ->first();
    }

    /**
     * create new user in table
    */
    public function createUser($data_user){
        $salt = $this->generationSalt();
        $pass = $this->generationPassword($data_user['password'],$salt);
         DB::table('customers')
            ->insert(
                [
                    'sex'=>$data_user['sex'],
                    'last_name'=>$data_user['last_name'],
                    'fast_name'=>$data_user['fast_name'],
                    'country'=>$data_user['country'],
                    'company'=>$data_user['company'],
                    'postcode'=>$data_user['postcode'],
                    'addsess'=>$data_user['addsess'],
                    'mail' => $data_user['mail'],
                    'phone'=>$data_user['phone'],
                    'salt'=>$salt,
                    'password'=>$pass,
                ]
        );

        $approved_user = $this->checkEmail($data_user['mail']);
        $approved_user = $approved_user[0];
        Session::put('user_info', $approved_user);
        return true;
    }

    public function generationPassword($pass, $salt = "") {
        return md5($salt . $pass);
    }

    public function generationSalt() {
        $str = md5(microtime());
        return substr($str, -6);
    }

    /**
     * @return all user info
     * */
    public function getUserData($email){
        return DB::table('customers')
            ->where('mail',$email)
            ->first();
    }

    /**
     * @return list country in table web_country_city
     *
    */
    public function getCountry(){
        return DB::table('web_country_city')
            ->where('status',0)
            ->get();
    }
    /**
     * @return all active coupons 
    */
    public function getByCouponsList(){
        return DB::table('coupons')
            ->where('count','>',0)
            ->select('*')
            ->where('active',1)
            ->get();
    }

    /**
     * @return new data of user
     * change user data by request
     */
    public function changeUserData($request){
        $user = Session::get('user_info');

        DB::table('customers')
            ->where('id', $user->id)
            ->update([
                'last_name'=>$request['last_name'],
                'fast_name'=>$request['fast_name'],
                'phone'=>$request['phone'],
                'country'=>$request['country'],
            ]);


        $approved_user = $this->checkEmail($request['mail']);
        $approved_user = $approved_user[0];
        Session::put('user_info', $approved_user);
        return true;
    }


    public function getUserOrdersData(){
        $user = Session::get('user_info');
        $dr =  DB::table('orders')
                    ->where('user_id', $user->id)
                    ->select('*')
                    ->orderBy('id','desc')
                    ->get();
        return $dr;
    }
    public function getUserOrdersDataID($ord){
        $user = Session::get('user_info');
        $dr =  DB::table('orders')
            ->where('user_id', $user->id)
            ->where('order_id', $ord)
            ->select('*')
            ->orderBy('id','desc')
            ->get();
        return $dr;
    }
    public function getUserOrderId($id){
        $user = Session::get('user_info');
        $dr =  DB::table('orders')
            ->where('user_id', $user->id)
            ->where('order_id', $id)
            ->select('*')
            ->first();
        return $dr;
    }

    /**
     * @param $request
     * @return image path
     * create and change file by user
     */
    public function changeUserPhoto($request){
        $user = Session::get('user_info');
        $str = md5(microtime());
        if(Image::make($request['photo'])->resize(235, 235)->save('public/uploads/user_photo/'.$str.'.jpg')){
            DB::table('customers')
                ->where('id', $user->id)
                ->update([
                    'photo'=>$str.'.jpg',
                ]);
            return 'public/uploads/user_photo/'.$str.'.jpg';
        }
        else{
            return false;
        }


    }

    public function uploadProductPhoto($request){
        $user = Session::get('user_info');
        $str = md5(microtime());
        $time_path = '/'.date('Y-m-dHis').$str.'.jpg';
        if(Image::make($request['photo'])->save('storage/uploads'.$time_path.'.png')){
            $photo =  DB::table('uploads')
                            ->insert([
                                'name'=>$str.'.png',
                                'path'=>$time_path,
                                'extension'=>'png',
                                'user_id'=>'1',
                                'hash'=>'rbn7couy0fp4u5rtvzsm',
                                'public'=>'1',
                                'created_at'=>date('Y-m-d'),
                                'updated_at'=>date('Y-m-d'),
                            ]);
            return '/storage/uploads'.$time_path.'.png';
        }
        else{
            return false;
        }


    }
    /**
     *  @return array orders
     */
    public function getOrders(){
        $user = Session::get('user_info');
        return DB::table('orders_customers as oc')
            ->where('email', $user->mail)
            ->leftJoin('orders_items as oi', 'oi.order_number', '=', 'oc.order_number')
            ->leftJoin('orders as o', 'o.order_number', '=', 'oc.order_number')
            ->select('oi.name as orders_name','oi.product_type','oi.game')
            ->get();
        ///ALTER TABLE  `gdb`.`orders` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci
    }

    /**
     * @return array data of levels
     */
    public function getAccountLevels(){

        return DB::table('account_levels as al')
            ->where('al.deleted_at',null)
            ->leftJoin('uploads as u', 'u.id', '=', 'al.cover')
            ->select('al.*','u.name as photo_name','u.hash')
            ->groupBy('al.id')
            ->get();

    }

    /**
     * @return array coupons
     */
    public function getActiveCoupons(){
        $user = Session::get('user_info');

        return DB::table('user_coupons as uc')
            ->where('user_id', $user->mail)
            ->leftJoin('coupons as c', 'c.code', '=', 'uc.code')
            ->select('c.*','uc.date_set')
            ->get();
    }


    /**
     * @return array coupons
     */
    public function activateCouponByCode($code){
        $user = Session::get('user_info');

        $coupon = DB::table('coupons')
                        ->where('code', $code)
                        ->select('*')
                        ->first();

        if($coupon->count>0){

            /**
             * if coupon exist in current user
             */
            $has_coupon = DB::table('user_coupons')
                ->where('code', $code)
                ->where('user_id',$user->mail)
                ->select('*')
                ->first();

            if($has_coupon){
                return false;
            }
            /**
             * update current coupon
             */
            DB::table('coupons')
                ->where('id',$coupon->id)
                ->update(
                    [
                        'count'=>($coupon->count-1)
                    ]
                );
            /**
             * add coupon in current user
             */
             DB::table('user_coupons')
                        ->insert(
                            [
                                'user_id' => $user->mail,
                                'code'=>$code,
                                'date_set'=>date('Y-m-d')
                            ]
                        );
            /**
             * return current coupon
             */
            return DB::table('user_coupons as uc')
                        ->where('uc.code', $code)
                        ->where('uc.user_id', $user->mail)
                        ->leftJoin('coupons as c', 'c.code', '=', 'uc.code')
                        ->select('c.*','uc.date_set')
                        ->first();
        }
        else{
            return false;
        }


    }
}
