$(document).ready(function() {

	if ($('.opened_menu').length>0) {
		$('.opened_menu .menus ul li').css('width', '100%');
	}

	$('.open_search').on('click', function(event) {
		event.preventDefault();
		console.log('test');
		$('#search_form').toggle();
	});

	$('.big_slider-wrp').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.big_slider-wrp').slick({
		arrows:false,
		dots:true,
		autoplay: true,
  		autoplaySpeed: 5000,
	});

	if ($(window).width()<1199) {
		$('.menu>ul>li:nth-child(n+2) a').on('click', function(event) {
			event.preventDefault();
			console.log('test');
			$(this).siblings('.hidden_menu').toggle();
		});
	}
	

	$('.aanbevolen_product-carousel').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.aanbevolen_product-carousel').slick({
		  dots: false,
		  arrows:true,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
	});

	$('.populairste_producten-carousel').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.populairste_producten-carousel').slick({
		  dots: false,
		  arrows:true,
		  slidesToShow: 6,
		  slidesToScroll: 6,
		  responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 5,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 4
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
	});

	$('.comments-carousel').on('init', function () {
        $(this).css('visibility', 'visible');
    });

	$('.comments-carousel').slick({
		arrows:true,
		dots:false,
		adaptiveHeight: true,
	});

	$('.links .row div').each(function(index, el) {
		if($(this).height()>238) {
			$(this).css({
				'height' : '238px',
				'overflow' : 'hidden',
			});
			$(this).append("<span><a href='' class='more_links'>more...</a><span>");
		};
	});
	$('.more_links').on('click', function(event) {
		event.preventDefault();
		$(this).closest('span').parent('div').css({
			'height' : 'auto'
		});
		$(this).closest('span').remove();
	});

	$('.login').on('click', function(event) {
		event.preventDefault();
		$('#LoginModal').arcticmodal({
            openEffect: { 
                type: 'fade',
                speed: 500 
            },
            closeEffect: {
                type: 'fade', 
                speed: 500 
            },
            afterOpen: function(data, el) {
                $('html').css('overflow-y', 'hidden');
            },
            beforeClose: function(data, el) {
                $('html').css('overflow-y', 'auto');
            },
        });
	});
	
	$('.open_menu').on('click', function(event) {
		event.preventDefault();
		$(this).siblings('ul').slideToggle();
	});

	/*    map      */
	
	var map;
     	function initMap() {
     		 var img = {
		      url: '/img/pin.png',
		    };
     		var myLatlng = new google.maps.LatLng(52.0009833, 4.1613421);
     		var mapOptions = {
		      zoom: 18,
		      scrollwheel: false,
		      center: myLatlng ,
		    };
			map = new google.maps.Map(document.getElementById('map'),mapOptions );
			var marker = new google.maps.Marker({
		      position: myLatlng,
		      map: map,
		      icon: img,
		    });
		};
		if ($('#map').length > 0) {
			initMap();
		}

  		/*  select   */
  		
  		$(".js-example-basic-single").select2();

  		// change location in some time
		   /*setTimeout('location.replace("/html/main.html")', 3000);
			$(window).on('load', function() {
			      setTimeout('location.replace("/html/main.html")', 3000);
			});*/

});